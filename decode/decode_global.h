#ifndef DECODE_GLOBAL_H
#define DECODE_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(DECODE_LIBRARY)
#  define DECODESHARED_EXPORT Q_DECL_EXPORT
#else
#  define DECODESHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // DECODE_GLOBAL_H
