#pragma once

#include <mutex>
#include <functional>
#include "MediaMeta.h"
#include "DecodeHandler.h"
#include "EncodeHandler.h"
#include "decode_global.h"

namespace smg_core
{
	typedef std::vector<AVCodecParameters*> CodeParamVec;
	typedef std::function<void(std::string)> CmdCallback;

    class DECODESHARED_EXPORT MediaManager
	{
	private:
		MediaManager();
		virtual ~MediaManager();

	public:
		static MediaManager* instance();

	public:
        int open_media(const char* input_url, const char* output_url = nullptr, EMediaStreamType stream_type = eMediaStream_realtime, EStreamActionType action_type = eStreamAction_push_stream);
		int close_media(const char* in_out_url);

	public:
		MediaMeta* find_media_meta(const char* url);
		DecodeHandler* find_decode_handler(const char* url, bool b_create = false);
		EncodeHandler* find_encode_handler(const char* url, bool b_create = false);

	public:
		//decode
		int execute_decode(const char* url, int media_type = (eSupMedia_video | eSupMedia_audio));
		int operate_decode(const char* url, EDECodeOperType oper);

		//encode
		int execute_encode(const char* url, int media_type = (eSupMedia_video | eSupMedia_audio));
		int operate_encode(const char* url, EDECodeOperType oper);

	public:
		//bts ets单位: ms
		int config_clip_parameter(const char* url, int64_t bts, int64_t ets = -1);

	public:
		const char* query_error_description(int err_code);

		int query_media_progress(const char* url, int64_t& cur_ts);
		int adjust_media_progress(const char* url, int64_t seek_ts);
		int query_media_duration(const char* url, int64_t& duration);

	private:
		int initialize_filter();
		int finish_push_stream(const char* in_out_url);
		int remove_dencode_handle(const char* in_out_url);

	private:
		int open_input_media(const char* url, MediaMeta& meta);
		int open_output_media(const char* url, MediaMeta& meta);
		int open_decoder(const AVCodecParameters* codec_par, AVCodecContext*& codec_ctx);

	private:
		int fill_encoder(const AVCodecContext* src, AVCodecContext* dst);

	public:
		inline EStreamMediaType query_stream_media_type(const char* url, const char*& key_str);

	private:
		const char* _rtsp_key_str;
		const char* _rtmp_key_str;
		const char* _rtsp_addr_str;
		const char* _rtmp_addr_str;

	private:
		std::mutex _media_meta_mtx;
		MediaMetaMap _media_meta_map;

		std::mutex _decode_handler_mtx;
		DecodeHndlMap _decode_handler_map;

		std::mutex _encode_handler_mtx;
		EncodeHndlMap _encode_handler_map;
	};
}

