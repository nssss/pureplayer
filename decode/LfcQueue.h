
#pragma once

#include <atomic>
#include <cstddef>

namespace smg_core
{
	template<typename T, size_t SIZE = 1024>
	class LfcQueue
	{
	public:
		LfcQueue() : _tail(0), _head(0){}
		virtual ~LfcQueue() {}

	public:
		LfcQueue<T, SIZE>& operator = (const LfcQueue<T, SIZE>& que);

	public:
		bool push(const T& item); // pushByMOve?
		bool pop(T& item);

	public:
		bool clear();
		bool empty() const;
		bool full() const;
		bool lock_free() const;

	private:
		size_t increment(size_t idx) const;

	private:
		enum { Capacity = SIZE + 1 };
		T _array[Capacity];

		std::atomic <size_t>  _head;  // head(output) index
		std::atomic <size_t>  _tail;  // tail(input) index
	};

	template<typename T, size_t SIZE>
	LfcQueue<T, SIZE>& LfcQueue<T, SIZE>::operator = (const LfcQueue<T, SIZE>& que)
	{
		if (this == &que){
			return *this;
		}

		size_t current_head = que._head.load();
		size_t current_tail = que._tail.load();

		memcpy(_array, que._array, sizeof(T) * Capacity);

		_head.store(current_head);
		_tail.store(current_tail);

		return *this;
	}

	template<typename T, size_t SIZE>
	bool LfcQueue<T, SIZE>::push(const T& item)
	{
		const size_t current_tail = _tail.load(std::memory_order_relaxed);
		const size_t next_tail = increment(current_tail);
		if (next_tail == _head.load(std::memory_order_acquire)){   // full queue
			return false;
		}

		_array[current_tail] = item;
		_tail.store(next_tail, std::memory_order_release);
		return true;
	}

	// Pop by Consumer can only update the head (load with relaxed, store with release)
	// the tail must be accessed with at least aquire
	template<typename T, size_t SIZE>
	bool LfcQueue<T, SIZE>::pop(T& item)
	{
		const size_t current_head = _head.load(std::memory_order_relaxed);
		if (current_head == _tail.load(std::memory_order_acquire)){   // empty queue
			return false;
		}

		item = _array[current_head];
		_head.store(increment(current_head), std::memory_order_release);
		return true;
	}

	template<typename T, size_t SIZE>
	bool LfcQueue<T, SIZE>::clear()
	{
		const size_t current_head = _head.load(std::memory_order_relaxed);
		_head.store(0, std::memory_order_release);

		const size_t current_tail = _tail.load(std::memory_order_relaxed);
		_tail.store(0, std::memory_order_release);

		return empty();
	}

	template<typename T, size_t SIZE>
	bool LfcQueue<T, SIZE>::empty() const
	{
		return (_head.load() == _tail.load());    // snapshot with acceptance of that this comparison operation is not atomic
	}

	// snapshot with acceptance that this comparison is not atomic
	template<typename T, size_t SIZE>
	bool LfcQueue<T, SIZE>::full() const
	{
		const auto next_tail = increment(_tail.load()); // aquire, we dont know who call
		return (next_tail == _head.load());
	}

	template<typename T, size_t SIZE>
	bool LfcQueue<T, SIZE>::lock_free() const
	{
		return (_tail.is_lock_free() && _head.is_lock_free());
	}

	template<typename T, size_t SIZE>
	size_t LfcQueue<T, SIZE>::increment(size_t idx) const
	{
		return (idx + 1) % Capacity;
	}
}