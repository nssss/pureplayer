#pragma once

#include <atomic>
#include <string>
#include <vector>
#include <thread>
#include <future>
#include <unordered_set>
#include <unordered_map>

#include "MediaMeta.h"
#include "../common/CommDef.h"
#include "decode_global.h"

namespace smg_core
{
	//解码支持的媒体类型
	typedef enum ESupMediaType{
		eSupMedia_video    = 0x01,
		eSupMedia_audio    = 0x02,
		eSupMedia_subtitle = 0x04,
		eSupMedia_all      = 0x07,
	}ESupMediaType;

	typedef std::unordered_set<ESupMediaType> MediaTypeSet;
	typedef std::unordered_map<AVMediaType, ESupMediaType> MediaTypeMap;
	typedef std::function<int(void*)> TimeoutCallback;

    class DECODESHARED_EXPORT DecodeHandler
	{
	public:
		DecodeHandler(MediaMeta* meta = nullptr);
		virtual ~DecodeHandler();

	public:
		int execute_decode(const MediaMeta* meta, int media_type);
		int keep_decode();
		int pause_decode();
		int exit_decode();

	public:
		int query_media_duration(int64_t& duration);
		int query_media_current_time(int64_t& cur_time);
		static int timeout_callback(void* param);
		int adjust_media_progress(int64_t cur_time);

	private:
		int read_frame();
		int seek_media();
		int finish_decode();
		int decode_frame(AVCodecContext* ctx, AVPacket* pkt, AVFrame* frm);

	private:
		int tick();
		void write_log(int err_code);
		void parse_sup_media_type(int media_type);

	private:
		void state_machine_thread();
		void frame_decode_thread(void* param);

	private:
		std::thread _state_th;
		std::thread _decode_th;
		std::promise<int> _state_ret_val;
		std::promise<int> _decode_ret_val;

		std::atomic<bool> _b_quit;
		std::atomic<bool> _b_pause;

	private:
		AVPacket _media_pkt;
		int _media_decode_type;
		MediaMeta* _media_meta;

        int64_t _seek_to_ts;
		std::atomic<int64_t> _media_cur_time;

		MediaTypeSet _sup_media_set;
		MediaTypeMap _media_type_map;

	private:
		typedef enum EStateType{
			eState_undef    = -1,
			eState_pause    = 0,
			eState_read     = 1,
			eState_forward  = 2,
			eState_backward = 3,
			eState_finish   = 4,
		}EStateType;

		std::atomic<EStateType> _state_code;
	};

	typedef std::unordered_map<std::string, DecodeHandler*> DecodeHndlMap;
}


