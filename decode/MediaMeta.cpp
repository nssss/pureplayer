#include "MediaMeta.h"
using namespace smg_core;

//StreamInfo
StreamInfo::StreamInfo()
{
	for (int i = 0; i < eInOut_SIZE; ++i){
		_stream[i] = nullptr;
		_codec_ctx[i] = nullptr;
		_realtime_pts[i] = 0;
	}

	_pkt_index = 0;
	_stream_index = -1;
	_seek_offset_pts = 0;
	_stream_duration = -1;

    _b_set_pkt_dts = false;
    _b_pkt_apply_offset = false;

	_media_type = AVMEDIA_TYPE_UNKNOWN;

	_pkt_callback = nullptr;
	_frm_callback = nullptr;
}

StreamInfo::StreamInfo(int stream_index, AVMediaType media_type, int64_t duration)
{
	for (int i = 0; i < eInOut_SIZE; ++i){
		_stream[i] = nullptr;
		_codec_ctx[i] = nullptr;
		_realtime_pts[i] = 0;
	}

	_pkt_index = 0;
	_seek_offset_pts = 0;

    _b_set_pkt_dts = false;
    _b_pkt_apply_offset = false;

	_media_type = media_type;
	_stream_index = stream_index;
    _stream_duration = duration;

	_pkt_callback = nullptr;
	_frm_callback = nullptr;
}

StreamInfo::StreamInfo(const StreamInfo& info)
{
	*this = info;
}

StreamInfo::~StreamInfo()
{

}

StreamInfo& StreamInfo::operator = (const StreamInfo& info)
{
	if (this == &info){
		return *this;
	}

	for (int i = 0; i < eInOut_SIZE; ++i){
		_stream[i] = info._stream[i];
		_codec_ctx[i] = info._codec_ctx[i];
		_realtime_pts[i] = info._realtime_pts[i];
	}

	_pkt_que = info._pkt_que;

	_pkt_index = info._pkt_index;
	_media_type = info._media_type;
	_stream_index = info._stream_index;
	_seek_offset_pts = info._seek_offset_pts;
	_stream_duration = info._stream_duration;

    _b_set_pkt_dts = info._b_set_pkt_dts;
    _b_pkt_apply_offset = info._b_pkt_apply_offset;

	_pkt_callback = info._pkt_callback;
	_frm_callback = info._frm_callback;

	return *this;
}

//MediaMeta
MediaMeta::MediaMeta() 
	: _inout_url_split_syb("|")
    , _action_type(eStreamAction_undef)
	, _media_stream_type(eMediaStream_undef)
{
	_duration = -1;
	_start_decode_time = -1;
	_fin_callback = nullptr;

	_encode_mode = eEncode_fast;
    _b_has_audio_stream = false;
    _b_has_video_stream = false;

	_format_ctx[eInOut_in] = nullptr;
	_format_ctx[eInOut_out] = nullptr;

    _in_out_media_type[eInOut_in] = eMedia_undef;
    _in_out_media_type[eInOut_out] = eMedia_undef;
}

MediaMeta::MediaMeta(const MediaMeta& meta)
{
	*this = meta;
}

MediaMeta::~MediaMeta()
{

}

MediaMeta& MediaMeta::operator = (const MediaMeta& meta)
{
	if (this == &meta){
		return *this;
	}

	_input_url = meta._input_url;
	_output_url = meta._output_url;

	_duration = meta._duration;
	_action_type = meta._action_type;
	_encode_mode = meta._encode_mode;
	_fin_callback = meta._fin_callback;
	_stream_info_vec = meta._stream_info_vec;

    _b_has_audio_stream = meta._b_has_audio_stream;
    _b_has_video_stream = meta._b_has_video_stream;

	_media_stream_type = meta._media_stream_type;
	_start_decode_time = meta._start_decode_time;

	_format_ctx[eInOut_in] = meta._format_ctx[eInOut_in];
	_format_ctx[eInOut_out] = meta._format_ctx[eInOut_out];

    _in_out_media_type[eInOut_in] = meta._in_out_media_type[eInOut_in];
    _in_out_media_type[eInOut_out] = meta._in_out_media_type[eInOut_out];

	return *this;
}

StreamInfo* MediaMeta::find_stream_info(AVMediaType media_type)
{
	StreamInfo* info = nullptr;
	for (int i = 0; i < _stream_info_vec.size(); ++i){
		if (_stream_info_vec[i]->_media_type == media_type){
			info = _stream_info_vec[i];
			break;
		}
	}

	return info;
}

std::string MediaMeta::generate_inout_url()
{
	std::string url = _input_url;
	//if (!_output_url.empty()){
	//	url += _inout_url_split_syb + _output_url;
	//}

	return url;
}

std::string MediaMeta::generate_inout_url(const char* input_url, const char* output_url)
{
	if (!output_url){
		return input_url;
	}

	char url[1024] = { 0 };
	sprintf_s(url, "%s%s%s", input_url, _inout_url_split_syb, output_url);
	return std::string(url);
}

bool MediaMeta::clear()
{
	bool ret = true;
	for (int i = 0; i < _stream_info_vec.size(); ++i){
		ret &= _stream_info_vec[i]->_pkt_que.clear();
	}

	return ret;
}

long MediaMeta::release()
{
	int ret = 0;
	StreamInfoVec::iterator it = _stream_info_vec.begin();
	for (; it != _stream_info_vec.end(); it = _stream_info_vec.erase(it)){
		StreamInfo* info = *it;
		for (int j = 0; j < eInOut_SIZE; ++j){
			if (info->_codec_ctx[j]){
				ret = avcodec_close(info->_codec_ctx[j]);
				avcodec_free_context(&info->_codec_ctx[j]);
			}
		}

		delete info;
	}

	AVFormatContext* ifmt = _format_ctx[eInOut_in];
	if (ifmt){
		avformat_close_input(&ifmt);
	}

	AVFormatContext* ofmt = _format_ctx[eInOut_out];
	if (ofmt){
		if (!(ofmt->oformat->flags & AVFMT_NOFILE)){
			ret = avio_closep(&ofmt->pb);
		}
		avformat_free_context(ofmt);
	}

	_duration = -1;
	_start_decode_time = -1;
	_fin_callback = nullptr;
	_encode_mode = eEncode_fast;

    _b_has_video_stream = false;
    _b_has_audio_stream = false;

	_format_ctx[eInOut_in] = nullptr;
	_format_ctx[eInOut_out] = nullptr;

    _in_out_media_type[eInOut_in] = eMedia_undef;
    _in_out_media_type[eInOut_out] = eMedia_undef;

	return 0;
}
