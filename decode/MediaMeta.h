#pragma once

#include <vector>
#include <cinttypes>
#include <functional>
#include <unordered_map>

#include "FFmpeg.h"
#include "LfcQueue.h"
#include "../common/CommDef.h"
#include "decode_global.h"

namespace smg_core
{
	typedef LfcQueue<AVFrame*> FrameQueue;
	typedef LfcQueue<AVPacket> PacketQueue;

	struct StreamInfo;
	typedef std::vector<StreamInfo*> StreamInfoVec;

	typedef std::function<int(const char*)> FinishCallback;
	typedef std::function<int(StreamInfo*, void*)> EncodeCallback;
	typedef EncodeCallback PacketCallback;
	typedef EncodeCallback FrameCallback;

	typedef enum EInOutType{ eInOut_in, eInOut_out, eInOut_SIZE }EInOutType;
    typedef enum EStreamMediaType{ eMedia_undef, eMedia_rtsp, eMedia_rtmp }EStreamMediaType;

    typedef struct DECODESHARED_EXPORT StreamInfo
	{
	public:
		explicit StreamInfo();
		explicit StreamInfo(int stream_index, AVMediaType media_type, int64_t duration);
		explicit StreamInfo(const StreamInfo& info);
		virtual ~StreamInfo();

	public:
		StreamInfo& operator = (const StreamInfo& info);

	public:
		PacketQueue _pkt_que;
		AVMediaType _media_type;

		int64_t _pkt_index;
		int16_t _stream_index;
		int64_t _stream_duration;
		int64_t _seek_offset_pts;

    public:
        bool _b_set_pkt_dts;                  //是否设置pkt->dts
        bool _b_pkt_apply_offset;          //是否添加offset

	public:
		int64_t _realtime_pts[eInOut_SIZE];
		const AVStream* _stream[eInOut_SIZE];
		AVCodecContext* _codec_ctx[eInOut_SIZE];

	public:
		FrameCallback _frm_callback;
		PacketCallback _pkt_callback;
	}StreamInfo;

	typedef enum EEncodeType{
		eEncode_fast = 0,        //快速
		eEncode_fine = 1,        //精细
	}EEncodeType;

    typedef struct DECODESHARED_EXPORT MediaMeta
	{
	public:
		explicit MediaMeta();
		explicit MediaMeta(const MediaMeta& meta);

		virtual ~MediaMeta();

	public:
		MediaMeta& operator = (const MediaMeta& meta);

	public:
		StreamInfo* find_stream_info(AVMediaType media_type);

		std::string generate_inout_url();
		std::string generate_inout_url(const char* input_url, const char* output_url = nullptr);

	public:
		bool clear();                //仅重置队列
		long release();

	public:
		std::string _input_url;
		std::string _output_url;
		const char* _inout_url_split_syb;

	public:
        EStreamActionType _action_type;
		EMediaStreamType _media_stream_type;
        int _in_out_media_type[eInOut_SIZE];

    public:
        bool _b_has_video_stream;
        bool _b_has_audio_stream;

	public:
		int64_t _duration;
		EEncodeType _encode_mode;
		int64_t _start_decode_time;
		FinishCallback _fin_callback;
		StreamInfoVec _stream_info_vec;
		AVFormatContext* _format_ctx[eInOut_SIZE];
	}MediaMeta;

	typedef std::unordered_map<std::string, MediaMeta> MediaMetaMap;
}


