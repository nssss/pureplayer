#pragma once

#include <string>
#include <unordered_map>

#include "MediaMeta.h"
#include "../common/CommDef.h"
#include "decode_global.h"

namespace smg_core
{
    class DECODESHARED_EXPORT EncodeHandler
	{
	public:
		EncodeHandler(MediaMeta* meta);
		virtual ~EncodeHandler();

	public:
		int initialize();
		int finish_push_stream();
		int encode_data(StreamInfo* info, void* pkt_frm);

	private:
		int fast_encode(StreamInfo* info, AVPacket* pkt);
		int fine_encode(StreamInfo* info, AVFrame* frm);

	private:
		int save_stream();
		int flush_stream(StreamInfo* info);

		int write_frame(StreamInfo* info, AVFrame* frm, bool* b_has_pkt = nullptr);
		int write_packet(StreamInfo* info, AVPacket* pkt);

	private:
		typedef struct FilterContext{
			AVFilterGraph* filter_graph;
			AVFilterContext* buf_src_ctx;
			AVFilterContext* buf_sink_ctx;
		}FilterContext;

	private:
		int init_filter();
		int init_media_filter(int index, const char *filter_spec);

	private:
		MediaMeta* _media_meta;
		FilterContext* _filter_ctx_arr;
	};

	typedef std::unordered_map<std::string, EncodeHandler*> EncodeHndlMap;
}
