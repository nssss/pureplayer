#include "EncodeHandler.h"
#include "MediaManager.h"
#include "../common/ErrorDescMgr.h"
using namespace smg_core;
using namespace smg_comm;

EncodeHandler::EncodeHandler(MediaMeta* meta)
{
	_media_meta = meta;
	_filter_ctx_arr = nullptr;
}

EncodeHandler::~EncodeHandler()
{

}

int EncodeHandler::initialize()
{
	return init_filter();
}

int EncodeHandler::init_filter()
{
	int ret = eSHErr_success;

	StreamInfoVec& stream_vec = _media_meta->_stream_info_vec;
	const int filter_arr_size = stream_vec.size() * sizeof(FilterContext);
	_filter_ctx_arr = (FilterContext*)av_malloc(filter_arr_size);
	if (!_filter_ctx_arr){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_av_malloc, eSHErr_logic_memory_alloc_failed);
	}

	memset(_filter_ctx_arr, 0, filter_arr_size);
	for (size_t i = 0; i < stream_vec.size(); ++i){
		AVMediaType media_type = stream_vec[i]->_media_type;
		if (media_type != AVMEDIA_TYPE_AUDIO && media_type != AVMEDIA_TYPE_VIDEO){
			continue;
		}

		const char *filter_spec = media_type == AVMEDIA_TYPE_VIDEO ? "null" : "anull";
		if ((ret = init_media_filter(i, filter_spec)) != eSHErr_success){
			break;
		}
	}

	return ret;
}

int EncodeHandler::init_media_filter(int index, const char *filter_spec)
{
	if (index < 0 || !filter_spec){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
	}

	FilterContext& filter_ctx = _filter_ctx_arr[index];
	StreamInfo* stream_info = _media_meta->_stream_info_vec[index];

	AVFilterGraph* filter_graph = avfilter_graph_alloc();
	if (!filter_graph){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avfilter_graph_alloc, eSHErr_logic_memory_alloc_failed);;
	}

	char args[1024] = { 0 };
	int ret = eSHErr_success;

	AVCodecContext* d_ctx = stream_info->_codec_ctx[eInOut_in];
	AVCodecContext* e_ctx = stream_info->_codec_ctx[eInOut_out];

	if (d_ctx->codec_type == AVMEDIA_TYPE_VIDEO) {
		sprintf_s(args, "video_size=%dx%d:pix_fmt=%d:time_base=%d/%d:pixel_aspect=%d/%d",
			d_ctx->width, d_ctx->height, d_ctx->pix_fmt,
			d_ctx->time_base.num, d_ctx->time_base.den,
			d_ctx->sample_aspect_ratio.num, d_ctx->sample_aspect_ratio.den
			);
	}
	else if (d_ctx->codec_type == AVMEDIA_TYPE_AUDIO){
		if (!d_ctx->channel_layout){
			d_ctx->channel_layout = av_get_default_channel_layout(d_ctx->channels);
		}

		sprintf_s(args, "time_base=%d/%d:sample_rate=%d:sample_fmt=%s:channel_layout=0x%x",
			d_ctx->time_base.num, d_ctx->time_base.den, d_ctx->sample_rate,
			av_get_sample_fmt_name(d_ctx->sample_fmt), d_ctx->channel_layout
			);
	}
	else{
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_media_not_support);
	}

	const char* cbufsrc = d_ctx->codec_type == AVMEDIA_TYPE_VIDEO ? "buffer" : "abuffer";
	const char* cbufsink = d_ctx->codec_type == AVMEDIA_TYPE_VIDEO ? "buffersink" : "abuffersink";

	const AVFilter *buf_src = avfilter_get_by_name(cbufsrc);
	const AVFilter *buf_sink = avfilter_get_by_name(cbufsink);
	if (!buf_src || !buf_sink) {
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avfilter_get_by_name, eSHErr_logic_arguments_invalid);
	}

	AVFilterContext *buf_src_ctx = nullptr;
	AVFilterContext *buf_sink_ctx = nullptr;
	if ((ret = avfilter_graph_create_filter(&buf_src_ctx, buf_src, "in", args, nullptr, filter_graph)) < 0){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avfilter_graph_create_filter, ret);
	}

	if ((ret = avfilter_graph_create_filter(&buf_sink_ctx, buf_sink, "out", nullptr, nullptr, filter_graph)) < 0){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avfilter_graph_create_filter, ret);
	}

	//参数设置
	if (d_ctx->codec_type == AVMEDIA_TYPE_VIDEO) {
		if ((ret = av_opt_set_bin(buf_sink_ctx, "pix_fmts", (uint8_t*)&e_ctx->pix_fmt, sizeof(e_ctx->pix_fmt), AV_OPT_SEARCH_CHILDREN)) < 0){
			return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_av_opt_set_bin, ret);
		}
	}
	else if (d_ctx->codec_type == AVMEDIA_TYPE_AUDIO) {
		if ((ret = av_opt_set_bin(buf_sink_ctx, "sample_fmts", (uint8_t*)&e_ctx->sample_fmt, sizeof(e_ctx->sample_fmt), AV_OPT_SEARCH_CHILDREN)) < 0){
			return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_av_opt_set_bin, ret);
		}

		if ((ret = av_opt_set_bin(buf_sink_ctx, "channel_layouts", (uint8_t*)&e_ctx->channel_layout, sizeof(e_ctx->channel_layout), AV_OPT_SEARCH_CHILDREN)) < 0){
			return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_av_opt_set_bin, ret);
		}

		if ((ret = av_opt_set_bin(buf_sink_ctx, "sample_rates", (uint8_t*)&e_ctx->sample_rate, sizeof(e_ctx->sample_rate), AV_OPT_SEARCH_CHILDREN)) < 0){
			return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_av_opt_set_bin, ret);
		}
	}

	AVFilterInOut *inputs = nullptr;
	AVFilterInOut *outputs = nullptr;
	if(!(inputs = avfilter_inout_alloc())){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avfilter_inout_alloc, eSHErr_logic_memory_alloc_failed);
	}

	if (!(outputs = avfilter_inout_alloc())){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avfilter_inout_alloc, eSHErr_logic_memory_alloc_failed);
	}

	inputs->name = av_strdup("out");
	if (!inputs->name){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_av_strdup, eSHErr_logic_memory_alloc_failed);
	}

	outputs->name = av_strdup("in");
	if (!outputs->name){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_av_strdup, eSHErr_logic_memory_alloc_failed);
	}

	inputs->filter_ctx = buf_sink_ctx;
	outputs->filter_ctx = buf_src_ctx;
	inputs->pad_idx = outputs->pad_idx = 0;
	inputs->next = outputs->next = nullptr;

	if ((ret = avfilter_graph_parse_ptr(filter_graph, filter_spec, &inputs, &outputs, nullptr)) < 0){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avfilter_graph_parse_ptr, ret);
	}

	if ((ret = avfilter_graph_config(filter_graph, nullptr)) < 0){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avfilter_graph_config, ret);
	}

	filter_ctx.buf_src_ctx = buf_src_ctx;
	filter_ctx.buf_sink_ctx = buf_sink_ctx;
	filter_ctx.filter_graph = filter_graph;

	avfilter_inout_free(&inputs);
	avfilter_inout_free(&outputs);

	return eSHErr_success;
}

int EncodeHandler::encode_data(StreamInfo* info, void* pkt_frm)
{
	if (!info || !pkt_frm){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
	}

	return _media_meta->_encode_mode == eEncode_fast ?
		fast_encode(info, (AVPacket*)pkt_frm) :
		fine_encode(info, (AVFrame*)pkt_frm);
}

int EncodeHandler::fast_encode(StreamInfo* info, AVPacket* pkt)
{
	const AVStream* in_stream = info->_stream[eInOut_in];
	const AVStream* out_stream = info->_stream[eInOut_out];
	if (!in_stream || !out_stream){
		av_packet_unref(pkt);
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_media_not_support);
	}

	AVRational time_base = in_stream->time_base;

	//没有时戳
	if (pkt->pts == AV_NOPTS_VALUE){
		int64_t frame_interval = (double)AV_TIME_BASE / av_q2d(in_stream->r_frame_rate);
		pkt->pts = info->_pkt_index * frame_interval / (av_q2d(time_base) * AV_TIME_BASE);
		pkt->duration = frame_interval / (av_q2d(time_base) * AV_TIME_BASE);
		pkt->dts = pkt->pts;
		++info->_pkt_index;
	}

	if (pkt->dts == AV_NOPTS_VALUE){
		pkt->dts = 0;
	}

	AVFormatContext* ofmt = nullptr; 
	if (!(ofmt = _media_meta->_format_ctx[eInOut_out])){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_null_pointer);
	}

	//pts/dts基于不同输入输出流时基的转换
	pkt->pts = av_rescale_q_rnd(pkt->pts, in_stream->time_base, out_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
	pkt->dts = av_rescale_q_rnd(pkt->dts, in_stream->time_base, out_stream->time_base, (AVRounding)(AV_ROUND_NEAR_INF | AV_ROUND_PASS_MINMAX));
	pkt->duration = av_rescale_q(pkt->duration, in_stream->time_base, out_stream->time_base);
    pkt->pos = -1;

    return av_interleaved_write_frame(ofmt, pkt);
}

int EncodeHandler::fine_encode(StreamInfo* info, AVFrame* frm)
{
	int ret = eSHErr_success;
	//push the decoded frame into the filtergraph
	FilterContext& filter_ctx = _filter_ctx_arr[info->_stream_index];
	if (ret = av_buffersrc_add_frame_flags(filter_ctx.buf_src_ctx, frm, 0) < 0){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_av_buffersrc_add_frame_flags, ret);
	}

	//pull filtered frames from the filtergraph
	AVFrame* e_frame = av_frame_alloc();
	if (!e_frame){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_av_frame_alloc, eSHErr_logic_memory_alloc_failed);
	}

	while (true) {
		/* if no more frames for output - returns AVERROR(EAGAIN)
		 * if flushed and no more frames for output - returns AVERROR_EOF
		 * rewrite retcode to 0 to show it as normal procedure completion
		 */
		if ((ret = av_buffersink_get_frame(filter_ctx.buf_sink_ctx, e_frame)) < 0){
			if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF){
				ret = eSHErr_success;
			}
			break;
		}

		e_frame->pict_type = AV_PICTURE_TYPE_NONE;
		if ((ret = write_frame(info, e_frame)) != eSHErr_success){
			break;
		}
	}

	av_frame_free(&e_frame);

	return ret;
}

int EncodeHandler::write_frame(StreamInfo* info, AVFrame* frm, bool* b_has_pkt)
{
	AVPacket e_pkt;
	av_init_packet(&e_pkt);

	int ret = eSHErr_success;
	AVCodecContext* e_ctx = info->_codec_ctx[eInOut_out];

	if (ret = avcodec_send_frame(e_ctx, frm) < 0){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avcodec_send_frame, ret);
	}

	while (ret >= 0){
		if ((ret = avcodec_receive_packet(e_ctx, &e_pkt)) < 0){
			if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF){
				if (b_has_pkt){
					*b_has_pkt = false;
				}
				ret = eSHErr_success;
			}
			else{
				ret = ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avcodec_receive_packet, ret);
			}

			break;
		}

		if (b_has_pkt){
			*b_has_pkt = true;
		}

		if ((ret = write_packet(info, &e_pkt)) != eSHErr_success){
			break;
		}

		av_packet_unref(&e_pkt);
	}

	return ret;
}

int EncodeHandler::write_packet(StreamInfo* info, AVPacket* pkt)
{
	// prepare packet for muxing
	pkt->stream_index = info->_stream_index;
	AVCodecContext* e_ctx = info->_codec_ctx[eInOut_out];
	const AVStream* out_stream = info->_stream[eInOut_out];
	AVFormatContext* ofmt = _media_meta->_format_ctx[eInOut_out];

	if (!e_ctx || !out_stream || !ofmt){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_null_pointer);
	}

	int ret = eSHErr_success;
	av_packet_rescale_ts(pkt, e_ctx->time_base, out_stream->time_base);

	//mux encoded frame 
	if ((ret = av_interleaved_write_frame(ofmt, pkt)) < 0){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_av_interleaved_write_frame, ret);
	}

	return ret;
}

int EncodeHandler::flush_stream(StreamInfo* info)
{
	AVCodecContext* e_ctx = info->_codec_ctx[eInOut_out];
	if (!e_ctx || !e_ctx->codec || !(e_ctx->codec->capabilities & AV_CODEC_CAP_DELAY)){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
	}

	bool b_has_pkt = true;
	int ret = eSHErr_success;

	while (true) {
		if ((ret = write_frame(info, nullptr, &b_has_pkt)) < 0){
			break;
		}

		if (!b_has_pkt){
			ret = eSHErr_success;
			break;
		}
	}

	return ret;
}

int EncodeHandler::save_stream()
{
	AVFormatContext* ofmt = _media_meta->_format_ctx[eInOut_out];
	if (!ofmt){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_null_pointer);
	}

	int ret = eSHErr_success;
	if ((ret == av_write_trailer(ofmt)) < 0){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_av_write_trailer, ret);
	}

	return ret;
}

int EncodeHandler::finish_push_stream()
{
	int ret = eSHErr_success;
	StreamInfoVec& stream_vec = _media_meta->_stream_info_vec;
	for (size_t i = 0; i < stream_vec.size(); ++i){
		if ((ret = flush_stream(stream_vec[i])) != eSHErr_success){
			break;
		}
	}

	if (ret == eSHErr_success){
		ret = save_stream();
	}

	return ret;
}
