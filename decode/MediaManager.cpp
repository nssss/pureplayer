
#include "MediaManager.h"
#include "../common/ErrorDescMgr.h"
using namespace smg_comm;
using namespace smg_core;

MediaManager::MediaManager()
	: _rtsp_key_str("rtsp")
	, _rtmp_key_str("flv")
	, _rtsp_addr_str("rtsp://")
	, _rtmp_addr_str("rtmp://")
{
	av_register_all();
	avformat_network_init();
}

MediaManager::~MediaManager()
{

}

MediaManager* MediaManager::instance()
{
	static MediaManager mgr;
	return &mgr;
}

int MediaManager::open_media(const char* input_url, const char* output_url, EMediaStreamType stream_type, EStreamActionType action_type)
{
	if (!input_url){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
	}

	std::unique_lock<std::mutex> m_lck(_media_meta_mtx);
	MediaMetaMap::iterator it = _media_meta_map.find(input_url);
	if (it != _media_meta_map.end()){
		return eSHErr_success;
	}
	m_lck.unlock();

	int ret = 0;
	MediaMeta meta;
	if ((ret = open_input_media(input_url, meta)) != eSHErr_success){
		return ret;
	}

	if (output_url && (ret = open_output_media(output_url, meta)) != eSHErr_success){
		return ret;
	}

	meta._input_url = input_url;
	FinishCallback fcb = std::bind(&MediaManager::finish_push_stream, this, std::placeholders::_1);
	meta._fin_callback = fcb;

	meta._action_type = action_type;
	meta._encode_mode = eEncode_fast;
	meta._media_stream_type = stream_type;
	std::string in_out_url = meta.generate_inout_url(input_url, /*output_url*/nullptr);

	m_lck.lock();
	_media_meta_map[in_out_url] = meta;
	m_lck.unlock();

	DecodeHandler* d_hndl = nullptr;
	if (!(d_hndl = new DecodeHandler(&_media_meta_map[in_out_url]))){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_memory_alloc_failed);
	}

	std::unique_lock<std::mutex> d_lck(_decode_handler_mtx);
	_decode_handler_map[in_out_url] = d_hndl;
	d_lck.unlock();

	if (output_url){
		EncodeHandler* e_hndl = nullptr;
		MediaMeta* tmp_meta = &_media_meta_map[in_out_url];
		if (!(e_hndl = new EncodeHandler(tmp_meta))){
			return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_memory_alloc_failed);
		}

		tmp_meta->_output_url = output_url;
		EncodeCallback ecb = std::bind(&EncodeHandler::encode_data, e_hndl, std::placeholders::_1, std::placeholders::_2);
		StreamInfoVec& stream_vec = tmp_meta->_stream_info_vec;
		for (size_t i = 0; i < stream_vec.size(); ++i){
			stream_vec[i]->_pkt_callback = ecb;
			//stream_vec[i]->_frm_callback = ecb;
		}

		std::unique_lock<std::mutex> e_lck(_encode_handler_mtx);
		_encode_handler_map[in_out_url] = e_hndl;
		e_lck.unlock();
	}

	return eSHErr_success;
}

int MediaManager::close_media(const char* in_out_url)
{
	if (!in_out_url){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
	}

	int ret = remove_dencode_handle(in_out_url);

	std::unique_lock<std::mutex> lck(_media_meta_mtx);
	MediaMetaMap::iterator it_m = _media_meta_map.find(in_out_url);
	if (it_m == _media_meta_map.end()){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_media_meta_not_found);
	}

	MediaMeta& meta = it_m->second;
	meta.release();
	_media_meta_map.erase(it_m);

	return eSHErr_success;
}

MediaMeta* MediaManager::find_media_meta(const char* url)
{
	if (!url){
		ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
		return nullptr;
	}

	std::unique_lock<std::mutex> lck(_media_meta_mtx);
	MediaMetaMap::iterator it = _media_meta_map.find(url);
	if (it == _media_meta_map.end()){
		ErrorDescMgr::instance()->record_error_code(eSHErr_logic_media_meta_not_found);
		return nullptr;
	}

	return &it->second;
}

DecodeHandler* MediaManager::find_decode_handler(const char* url, bool b_create)
{
	if (!url){
		ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
		return nullptr;
	}

	std::unique_lock<std::mutex> lck(_decode_handler_mtx);
	DecodeHndlMap::iterator it = _decode_handler_map.find(url);
	if (it != _decode_handler_map.end()){
		return it->second;
	}

	if (b_create){
		DecodeHandler* hndl = new DecodeHandler();
		if (!hndl){
			ErrorDescMgr::instance()->record_error_code(eSHErr_logic_memory_alloc_failed);
			return nullptr;
		}

		_decode_handler_map[url] = hndl;
		return hndl;
	}

	return nullptr;
}

EncodeHandler* MediaManager::find_encode_handler(const char* url, bool b_create)
{
	ErrorDescMgr::instance()->record_error_code(eSHErr_logic_method_not_implement);
	return nullptr;
}

//decode
int MediaManager::execute_decode(const char* url, int media_type)
{
	if (!url){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
	}

	std::unique_lock<std::mutex> lck(_decode_handler_mtx);
	DecodeHndlMap::iterator it = _decode_handler_map.find(url);
	if (it == _decode_handler_map.end()){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_decode_handler_not_found);
	}

	DecodeHandler* handler = it->second;
	lck.unlock();

	if (!handler){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_null_pointer);
	}

	return handler->execute_decode(nullptr, media_type);
}

int MediaManager::operate_decode(const char* url, EDECodeOperType oper)
{
	if (!url){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
	}

	std::unique_lock<std::mutex> lck(_decode_handler_mtx);
	DecodeHndlMap::iterator it = _decode_handler_map.find(url);
	if (it == _decode_handler_map.end()){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_decode_handler_not_found);
	}

	DecodeHandler* handler = it->second;
	lck.unlock();

	if (!handler){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_null_pointer);
	}

	int ret = 0;
	switch (oper){
	case eDECOper_keep: ret = handler->keep_decode(); break;
	case eDECOper_pause: ret = handler->pause_decode(); break;
	case eDECOper_exit: ret = handler->exit_decode(); break;
	}
	
	return ret;
}

//encode
int MediaManager::execute_encode(const char* url, int media_type)
{
	if (!url){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
	}

	return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_method_not_implement);
}

int MediaManager::operate_encode(const char* url, EDECodeOperType oper)
{
	if (!url){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
	}

	return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_method_not_implement);
}

//bts ets单位: ms
int MediaManager::config_clip_parameter(const char* url, int64_t bts, int64_t ets)
{
    if (!url || bts < 0 || (ets > 0 && bts > ets)){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
	}

	return 0;
}

int MediaManager::query_media_progress(const char* url, int64_t& cur_ts)
{
	if (!url){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
	}

	std::unique_lock<std::mutex> lck(_decode_handler_mtx);
	DecodeHndlMap::iterator it = _decode_handler_map.find(url);
	if (it == _decode_handler_map.end()){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_decode_handler_not_found);
	}

	DecodeHandler* handler = it->second;
	lck.unlock();

	if (!handler){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_null_pointer);
	}

	return handler->query_media_current_time(cur_ts);
}

int MediaManager::adjust_media_progress(const char* url, int64_t seek_ts)
{
	if (!url){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
	}

	std::unique_lock<std::mutex> lck(_decode_handler_mtx);
	DecodeHndlMap::iterator it = _decode_handler_map.find(url);
	if (it == _decode_handler_map.end()){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_decode_handler_not_found);
	}

	DecodeHandler* handler = it->second;
	lck.unlock();

	if (!handler){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_null_pointer);
	}

	return handler->adjust_media_progress(seek_ts);
}

int MediaManager::query_media_duration(const char* url, int64_t& duration)
{
	if (!url){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
	}

	std::unique_lock<std::mutex> lck(_decode_handler_mtx);
	DecodeHndlMap::iterator it = _decode_handler_map.find(url);
	if (it == _decode_handler_map.end()){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_decode_handler_not_found);
	}

	DecodeHandler* handler = it->second;
	lck.unlock();

	if (!handler){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_null_pointer);
	}

	return handler->query_media_duration(duration);
}

int MediaManager::initialize_filter()
{
	return 0;
}

int MediaManager::remove_dencode_handle(const char* in_out_url)
{
	std::unique_lock<std::mutex> d_lck(_decode_handler_mtx);
	DecodeHndlMap::iterator it_d = _decode_handler_map.find(in_out_url);
	if (it_d == _decode_handler_map.end()){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_decode_handler_not_found);
	}

	DecodeHandler* d = it_d->second;
	_decode_handler_map.erase(it_d);
	d_lck.unlock();
	delete d;

	std::unique_lock<std::mutex> e_lck(_encode_handler_mtx);
	EncodeHndlMap::iterator it_e = _encode_handler_map.find(in_out_url);
	if (it_e == _encode_handler_map.end()){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_encode_handler_not_found);
	}

	EncodeHandler* e = it_e->second;
	_encode_handler_map.erase(it_e);
	e_lck.unlock();
	delete e;

	return eSHErr_success;
}

int MediaManager::finish_push_stream(const char* in_out_url)
{
	std::unique_lock<std::mutex> e_lck(_encode_handler_mtx);
	EncodeHndlMap::iterator it_e = _encode_handler_map.find(in_out_url);
	if (it_e == _encode_handler_map.end()){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_encode_handler_not_found);
	}

	int ret = eSHErr_success;
	EncodeHandler* e_hndl = it_e->second;
	e_lck.unlock();

	if ((ret = e_hndl->finish_push_stream()) == eSHErr_success){
		ret = close_media(in_out_url);
	}

	return ret;
}

int MediaManager::open_input_media(const char* url, MediaMeta& meta)
{
	if (!url){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
	}

	int ret = 0;
	AVDictionary *dict = nullptr;
	const char* stream_key_str = nullptr;
	EStreamMediaType type = query_stream_media_type(url, stream_key_str);

	if (type == eMedia_rtsp && ((ret = av_dict_set(&dict, "rtsp_transport", "tcp", 0)) < 0 || (ret = av_dict_set(&dict, "stimeout", "10000000", 0) < 0))){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_av_dict_set, ret);
	}

	AVFormatContext* fmt_ctx = nullptr;
	if ((ret = avformat_open_input(&fmt_ctx, url, nullptr, &dict)) < 0){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avformat_open_input, ret);
	}

	if ((ret = avformat_find_stream_info(fmt_ctx, nullptr)) < 0){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avformat_find_stream_info, ret);
	}

	meta._duration = fmt_ctx->duration;
	meta._format_ctx[eInOut_in] = fmt_ctx;
    meta._in_out_media_type[eInOut_in] = type;

    bool b_sup_stream = false;
	CodeParamVec code_par_vec;
    for (int i = 0; i < fmt_ctx->nb_streams; ++i){
		const AVStream* in_stream = fmt_ctx->streams[i];
		AVCodecParameters* code_param = in_stream->codecpar;

        switch (code_param->codec_type){
        case AVMEDIA_TYPE_AUDIO: meta._b_has_audio_stream = true; b_sup_stream = true; break;
        case AVMEDIA_TYPE_VIDEO: meta._b_has_video_stream = true; b_sup_stream = true; break;
        default: b_sup_stream = false; break;
        }

        if (!b_sup_stream){ continue; }

        code_par_vec.push_back(code_param);
        int64_t duration = fmt_ctx->duration != AV_NOPTS_VALUE ? fmt_ctx->duration / 1000 :
                                      in_stream->duration * av_q2d(in_stream->time_base);                                          //单位：ms

        StreamInfo* stream_info = new StreamInfo(i, code_param->codec_type, duration);
        if(!stream_info){
                return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_memory_alloc_failed);
        }

        stream_info->_stream[eInOut_in] = in_stream;
        meta._stream_info_vec.push_back(stream_info);
	}

	AVCodecContext* tmp_codec_ctx = nullptr;
    for (int i = 0; i < code_par_vec.size(); ++i){
		if ((ret = open_decoder(code_par_vec[i], tmp_codec_ctx)) < 0){
			return ret;
		}

		meta._stream_info_vec[i]->_codec_ctx[eInOut_in] = tmp_codec_ctx;
	}

	return eSHErr_success;
}

int MediaManager::open_output_media(const char* url, MediaMeta& meta)
{
	if (!url){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
	}

	const char* stream_key_str = nullptr;
	EStreamMediaType type = query_stream_media_type(url, stream_key_str);

	int ret = 0;
	AVFormatContext* fmt_ctx;
	if ((ret = avformat_alloc_output_context2(&fmt_ctx, nullptr, stream_key_str, url)) < 0){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avformat_alloc_output_context, ret);
	}

	for (int i = 0; i < meta._stream_info_vec.size(); ++i){
		StreamInfo* stream_info = meta._stream_info_vec[i];
		if (stream_info->_media_type != AVMEDIA_TYPE_VIDEO && stream_info->_media_type != AVMEDIA_TYPE_AUDIO){
			continue;
		}

		AVStream* out_stream = avformat_new_stream(fmt_ctx, nullptr);
		if (!out_stream){
			return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avformat_new_stream, eSHErr_logic_memory_alloc_failed);
		}

		AVCodecContext* d_ctx = stream_info->_codec_ctx[eInOut_in];
		const AVStream* in_stream = stream_info->_stream[eInOut_in];

		AVCodec* e_codec = avcodec_find_encoder(d_ctx->codec_id);
		if (!e_codec){
			return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avcodec_find_encoder, eSHErr_logic_encode_handler_not_found);
		}

		AVCodecContext* e_ctx = avcodec_alloc_context3(e_codec);
		if (!e_ctx){
			return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avcodec_alloc_context, eSHErr_logic_memory_alloc_failed);
		}

		if ((ret = avcodec_parameters_to_context(e_ctx, in_stream->codecpar)) < 0){
			return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avcodec_parameters_to_context, ret);
		}

		switch (stream_info->_media_type){
		case AVMEDIA_TYPE_VIDEO:{
			fill_encoder(d_ctx, e_ctx);

			e_ctx->time_base = in_stream->time_base;
			e_ctx->sample_aspect_ratio = d_ctx->sample_aspect_ratio;
			e_ctx->pix_fmt = e_codec->pix_fmts ? e_codec->pix_fmts[0] : d_ctx->pix_fmt;

			//设置零延迟模式
			if ((ret = av_opt_set(e_ctx->priv_data, "tune", "zerolatency", 0)) != 0){
				return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_av_opt_set, ret);
			}

			break;
		}
		case AVMEDIA_TYPE_AUDIO:{
			// 设置通道数或channel_layout
			if (d_ctx->channels > 0 && d_ctx->channel_layout == 0) {
				d_ctx->channel_layout = av_get_default_channel_layout(d_ctx->channels);
			}
			else if (d_ctx->channels == 0 && d_ctx->channel_layout > 0) {
				d_ctx->channels = av_get_channel_layout_nb_channels(d_ctx->channel_layout);
			}

			e_ctx->channels = d_ctx->channels;
			e_ctx->channel_layout = d_ctx->channel_layout;
			e_ctx->sample_rate = d_ctx->sample_rate;

			e_ctx->time_base = { 1, e_ctx->sample_rate };
			e_ctx->sample_fmt = e_codec->sample_fmts[0];
			break;
		}
		default: break;
		}

		e_ctx->codec_tag = 0;
		if (fmt_ctx->oformat && (fmt_ctx->oformat->flags & AVFMT_GLOBALHEADER)){
			e_ctx->flags |= AV_CODEC_FLAG_GLOBAL_HEADER;
            fmt_ctx->oformat->flags |= AVFMT_TS_NONSTRICT;
		}

		if ((ret = avcodec_parameters_from_context(out_stream->codecpar, e_ctx)) < 0){
			return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avcodec_parameters_from_context, ret);
		}

        if ((ret = avcodec_open2(e_ctx, e_codec, nullptr)) < 0){
            return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avcodec_open, ret);
        }

		out_stream->time_base = e_ctx->time_base;
		stream_info->_stream[eInOut_out] = out_stream;
		stream_info->_codec_ctx[eInOut_out] = e_ctx;
	}

	if (fmt_ctx->oformat && !(fmt_ctx->oformat->flags & AVFMT_NOFILE)){
		if ((ret = avio_open(&fmt_ctx->pb, url, AVIO_FLAG_WRITE)) < 0){
			return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avio_open, ret);
		}
	}

	AVDictionary *dict = nullptr;
	if (type == eMedia_rtsp && (ret = av_dict_set(&dict, "rtsp_transport", "tcp", 0)) < 0){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_av_dict_set, ret);
	}

	if ((ret = avformat_write_header(fmt_ctx, &dict)) < 0){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avformat_write_header, ret);
	}

	meta._format_ctx[eInOut_out] = fmt_ctx;
    meta._in_out_media_type[eInOut_out] = type;

	return eSHErr_success;
}

int MediaManager::open_decoder(const AVCodecParameters* codec_par, AVCodecContext*& codec_ctx)
{
	AVCodecContext* tmp_ctx = avcodec_alloc_context3(nullptr);
	if (!tmp_ctx){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avcodec_alloc_context, eSHErr_logic_memory_alloc_failed);
	}

	int ret = 0;
	if ((ret = avcodec_parameters_to_context(tmp_ctx, codec_par)) < 0){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avcodec_parameters_to_context, ret);
	}

	AVCodec* codec = avcodec_find_decoder(tmp_ctx->codec_id);
	if (!codec){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avcodec_find_decoder, eSHErr_logic_decode_handler_not_found);
	}

	if ((ret = avcodec_open2(tmp_ctx, codec, nullptr)) < 0){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avcodec_open, ret);
	}

	codec_ctx = tmp_ctx;

	return eSHErr_success;
}

int MediaManager::fill_encoder(const AVCodecContext* src, AVCodecContext* dst)
{
	dst->gop_size = src->gop_size;

	dst->width = src->width;
	dst->height = src->height;
	dst->bit_rate = src->bit_rate;

	dst->refs = src->refs;                         // 3
	if (dst->refs == 0){
		dst->refs = 3;
	}

	dst->qmin = src->qmin;                         // 10
	if (dst->qmin == 0){
		dst->qmin = 10;
	}

	dst->qmax = src->qmax;                         // 51
	if (dst->qmax == 0){
		dst->qmax = 51;
	}

	dst->me_range = src->me_range;                 // 16
	if (dst->me_range == 0){
		dst->me_range = 16;
	}

	dst->max_qdiff = src->max_qdiff;               // 4
	if (dst->max_qdiff == 0){
		dst->max_qdiff = 4;
	}

	dst->qcompress = src->qcompress;               // 0.6
	if (dst->qcompress < 0.1){
		dst->qcompress = 0.6;
	}

	return eSHErr_success;
}

inline EStreamMediaType MediaManager::query_stream_media_type(const char* url, const char*& key_str)
{
	EStreamMediaType type = eMedia_undef;
	if (strstr(url, _rtsp_addr_str)){
		type = eMedia_rtsp;
		key_str = _rtsp_key_str;
	}
	else if (strstr(url, _rtmp_addr_str)){
		type = eMedia_rtmp;
		key_str = _rtmp_key_str;
	}
	return type;
}

const char* MediaManager::query_error_description(int err_code)
{
	return ErrorDescMgr::instance()->query_error_description(err_code);
}
