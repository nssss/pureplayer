
#include "DecodeHandler.h"
#include "MediaManager.h"
#include "../common/ErrorDescMgr.h"
using namespace smg_core;
using namespace smg_comm;

DecodeHandler::DecodeHandler(MediaMeta* meta)
{
	_b_quit = false;
	_b_pause = false;
	_media_meta = meta;

    _seek_to_ts = 0;
	_media_cur_time = 0;

	_state_code = eState_undef;

	_media_type_map[AVMEDIA_TYPE_VIDEO] = eSupMedia_video;
	_media_type_map[AVMEDIA_TYPE_AUDIO] = eSupMedia_audio;
	_media_type_map[AVMEDIA_TYPE_SUBTITLE] = eSupMedia_subtitle;
}

DecodeHandler::~DecodeHandler()
{

}

int DecodeHandler::execute_decode(const MediaMeta* meta, int media_type)
{
	if (!_media_meta && !meta){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_media_meta_not_set);
	}

	_media_decode_type = media_type;
	if (meta){
		_media_meta = const_cast<MediaMeta*>(meta);
	}

	_state_code = eState_read;
	_state_th = std::thread(std::bind(&DecodeHandler::state_machine_thread, this));
	_state_th.detach();

	parse_sup_media_type(media_type);

	for (int i = 0; i < _media_meta->_stream_info_vec.size(); ++i){
		StreamInfo* info = _media_meta->_stream_info_vec[i];
		MediaTypeMap::const_iterator it = _media_type_map.find(info->_media_type);
		if (it != _media_type_map.cend() && _sup_media_set.find(it->second) != _sup_media_set.end()){
			std::thread th = std::thread(std::bind(&DecodeHandler::frame_decode_thread, this, info));
			th.detach();
		}
	}
	
	//std::future<int> th_ret = _state_ret_val.get_future();
	//int ret = th_ret.get();

	return eSHErr_success;
}

int DecodeHandler::keep_decode()
{
	if (!_media_meta){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_media_meta_not_set);
	}

	_b_pause = false;
	_state_code = eState_read;
	std::this_thread::sleep_for(std::chrono::milliseconds(50));

	return eSHErr_success;
}

int DecodeHandler::pause_decode()
{
	if (!_media_meta){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_media_meta_not_set);
	}

	_b_pause = true;
	_state_code = eState_pause;
	std::this_thread::sleep_for(std::chrono::milliseconds(50));

	return eSHErr_success;
}

int DecodeHandler::exit_decode()
{
	if (!_media_meta){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_media_meta_not_set);
	}

	_state_code = eState_finish;
	std::this_thread::sleep_for(std::chrono::milliseconds(50));

	return eSHErr_success;
}

int DecodeHandler::seek_media()
{
    int ret = 0;
    if ((_seek_to_ts < 0 && _seek_to_ts != -1)){
            return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_seek_media_param_invalid);
    }

    //等待状态机进入暂停状态
    _state_code = eState_pause;
    std::this_thread::sleep_for(std::chrono::milliseconds(50));

    StreamInfoVec& info_vec = _media_meta->_stream_info_vec;
    for (size_t i = 0; i < info_vec.size(); ++i){
        StreamInfo* info = info_vec[i];
        if (info->_media_type != AVMEDIA_TYPE_VIDEO && info->_media_type != AVMEDIA_TYPE_AUDIO){
            continue;
        }

        int64_t end_ts = info->_stream_duration;
        if (_seek_to_ts > end_ts && end_ts > 0){
            return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_seek_media_param_invalid);
        }

        //flush packet
        avcodec_flush_buffers(info->_codec_ctx[eInOut_in]);

        //计算跳转时间戳
        const AVStream* stream = info->_stream[eInOut_in];
        int64_t seek_to_pts = _seek_to_ts / (double)1000 / av_q2d(stream->time_base);

        //set seek flag
        int seek_flag = _seek_to_ts > _media_cur_time ? AVSEEK_FLAG_FRAME : AVSEEK_FLAG_BACKWARD;

        //seek read stream
        if ((ret = av_seek_frame(_media_meta->_format_ctx[eInOut_in], info->_stream_index, seek_to_pts, seek_flag/*AVSEEK_FLAG_BACKWARD*/)) < 0){
            return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_av_seek_frame, ret);
        }

        //set output stream
        if (stream = info->_stream[eInOut_out]){
            avcodec_flush_buffers(info->_codec_ctx[eInOut_out]);
        }

        //set pts offset
        info->_seek_offset_pts = seek_to_pts;
        info->_b_set_pkt_dts = true;
    }

    //清除缓存数据
    _media_meta->clear();

    //继续读帧
    _state_code = eState_read;
    std::this_thread::sleep_for(std::chrono::milliseconds(50));

    return eSHErr_success;
}

int DecodeHandler::finish_decode()
{
	_b_quit = true;
	return eSHErr_success;
}

int DecodeHandler::query_media_duration(int64_t& duration)
{
	if (!_media_meta){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_media_meta_not_set);
	}

	StreamInfo* video = _media_meta->find_stream_info(AVMEDIA_TYPE_VIDEO);
	if (video){
		duration = video->_stream_duration;
		return eSHErr_success;
	}

	StreamInfo* audio = _media_meta->find_stream_info(AVMEDIA_TYPE_AUDIO);
	if (audio){
		duration = audio->_stream_duration;
		return eSHErr_success;
	}

	return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_specific_media_not_found);
}

int DecodeHandler::query_media_current_time(int64_t& cur_time)
{
	if (!_media_meta){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_media_meta_not_set);
	}

	cur_time = _media_cur_time;
	return eSHErr_success;
}

int DecodeHandler::timeout_callback(void* param)
{
	DecodeHandler* handler = (DecodeHandler*)param;
	return eSHErr_logic_timeout;
}

int DecodeHandler::adjust_media_progress(int64_t cur_time)
{
    _seek_to_ts = cur_time;
	return seek_media();
}

int DecodeHandler::read_frame()
{
	int ret = 0;
	AVFormatContext* fmt_ctx = _media_meta->_format_ctx[eInOut_in];
    if(!fmt_ctx){
           return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_null_pointer);
    }
	
	if ((ret = av_read_frame(fmt_ctx, &_media_pkt)) < 0){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_av_read_frame, ret);
	}

	//判断是否是支持的媒体类型
	if (_media_pkt.stream_index >= _media_meta->_stream_info_vec.size()){
		av_packet_unref(&_media_pkt);
		return eSHErr_success;
	}

    //计算当前播放时间
    StreamInfo* info = _media_meta->_stream_info_vec[_media_pkt.stream_index];
    bool b_handle_time = (_media_meta->_b_has_video_stream && info->_media_type == AVMEDIA_TYPE_VIDEO) ||
                         (!_media_meta->_b_has_video_stream && _media_meta->_b_has_audio_stream);

    if (b_handle_time){
        int64_t time_pts = _media_pkt.pts + (_media_meta->_action_type == eStreamAction_push_stream ? 0 : info->_seek_offset_pts);
        _media_cur_time = time_pts * 1000 * av_q2d(info->_stream[eInOut_in]->time_base);
    }

    //延时
    if (_media_meta->_media_stream_type == eMediaStream_playback){    //非rtsp/rtmp
        AVRational time_base_q = { 1, AV_TIME_BASE };
        AVRational time_base = info->_stream[eInOut_in]->time_base;

        int64_t fixed_offset = info->_b_pkt_apply_offset ? info->_seek_offset_pts : 0;
        int64_t pts_t = av_rescale_q(_media_pkt.pts + fixed_offset, time_base, time_base_q);
        int64_t base_offset = av_rescale_q(info->_seek_offset_pts, time_base, time_base_q);
        int64_t now_t = av_gettime() - _media_meta->_start_decode_time + base_offset;

        if (pts_t > now_t){
                int move_left_bit = _media_meta->_action_type == eStreamAction_push_stream ? 1 : 0;
                int delay = (pts_t - now_t) >> move_left_bit;
                av_usleep(delay);
        }
        else if (info->_b_set_pkt_dts && pts_t != AV_NOPTS_VALUE){
            int64_t diff_t = av_rescale_q(pts_t - now_t, time_base_q, time_base);
            info->_seek_offset_pts += diff_t;
        }

        if (info->_b_set_pkt_dts && _media_meta->_action_type == eStreamAction_pull_stream){
            info->_b_set_pkt_dts = false;
            if (_media_pkt.pts == AV_NOPTS_VALUE){
                info->_b_pkt_apply_offset = true;
            }
        }

        _media_pkt.pts += fixed_offset;
        _media_pkt.dts += fixed_offset;
    }

	//判断是否存在数据包处理回调函数
	if (info->_pkt_callback){
		ret = info->_pkt_callback(info, &_media_pkt);
	}
	else{     //存储到数据包缓存队列
		info->_pkt_que.push(_media_pkt);
	}

	return eSHErr_success;
}

int DecodeHandler::decode_frame(AVCodecContext* ctx, AVPacket* pkt, AVFrame* frm)
{
	int ret = eSHErr_success;
	if ((ret = avcodec_send_packet(ctx, pkt)) < 0){
		ret = eSHErr_continue;
	}
	else if ((ret = avcodec_receive_frame(ctx, frm)) < 0){
		if (ret == AVERROR(EAGAIN) || ret == AVERROR_EOF){
			ret = eSHErr_continue;
		}
		else{
			ret = ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_avcodec_receive_frame, ret);
		}
	}

	av_packet_unref(pkt);

	return ret;
}

void DecodeHandler::state_machine_thread()
{
	if (!_media_meta){
		ErrorDescMgr::instance()->record_error_code(eSHErr_logic_media_meta_not_set);
		return;
	}

	av_init_packet(&_media_pkt);
	_media_meta->_start_decode_time = av_gettime();

	AVFormatContext* ifctx = _media_meta->_format_ctx[eInOut_in];
	ifctx->interrupt_callback.opaque = this;
	ifctx->interrupt_callback.callback = timeout_callback;

	int ret = eSHErr_success;
	while (!_b_quit && ret == eSHErr_success){
		switch (_state_code){
		case eState_pause: ret = tick(); break;
		case eState_read: ret = read_frame(); break;
		case eState_forward: ret = seek_media(); break;
		case eState_backward: ret = seek_media(); break;
		case eState_finish: ret = finish_decode(); break;
		default: break;
		}
	}

	if (ret != eSHErr_success){
		ret = ErrorDescMgr::instance()->record_error_code(eSHErr_logic_state_machine_thread, ret);
	}

	if (_media_meta->_fin_callback){
		std::string in_out_url = _media_meta->generate_inout_url();
		ret = _media_meta->_fin_callback(in_out_url.c_str());
	}

	//_state_ret_val.set_value(ret);
}

void DecodeHandler::frame_decode_thread(void* param)
{
	if (!param){
		ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
		return;
	}

	AVFrame* d_frame = av_frame_alloc();
	if (!d_frame){
		ErrorDescMgr::instance()->record_error_code(eSHErr_logic_memory_alloc_failed);
		return;
	}

	AVPacket pkt;
	av_init_packet(&pkt);

	int ret = 0;
	StreamInfo* info = (StreamInfo*)param;
	AVCodecContext* d_ctx = info->_codec_ctx[eInOut_in];

	while (!_b_quit){
		if (_b_pause || !info->_pkt_que.pop(pkt)){
			//std::this_thread::yield();
			std::this_thread::sleep_for(std::chrono::milliseconds(50));
			continue;
		}

		if ((ret = decode_frame(d_ctx, &pkt, d_frame)) != eSHErr_success){
			if (ret == eSHErr_continue) continue;
			else break;
		}

		if (info->_frm_callback){
			info->_frm_callback(info, d_frame);
		}
	}

	av_frame_free(&d_frame);

	//_decode_ret_val.set_value(ret);
}

int DecodeHandler::tick()
{
    std::this_thread::sleep_for(std::chrono::milliseconds(20));
	return eSHErr_success;
}

void DecodeHandler::write_log(int err_code)
{

}

void DecodeHandler::parse_sup_media_type(int media_type)
{
	_sup_media_set.clear();
	if (media_type & eSupMedia_video){
		_sup_media_set.insert(eSupMedia_video);
	}

	if (media_type & eSupMedia_audio){
		_sup_media_set.insert(eSupMedia_audio);
	}

	if (media_type & eSupMedia_subtitle){
		_sup_media_set.insert(eSupMedia_subtitle);
	}
}
