#-------------------------------------------------
#
# Project created by QtCreator 2018-02-08T16:42:39
#
#-------------------------------------------------

QT       += xml

QT       -= gui

TARGET = common
TEMPLATE = lib

CONFIG(debug, debug|release){
    DESTDIR = $$PWD/../bin/debug/
}else{
    DESTDIR = $$PWD/../bin/release/
}

DEFINES += COMMON_LIBRARY

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
        common.cpp \
    ErrorDescMgr.cpp \
    ErrorDescription.cpp \
    LogUtil.cpp

HEADERS += \
        common.h \
        common_global.h \ 
    CommDef.h \
    ErrorDescMgr.h \
    ErrorDescription.h \
    LogUtil.h \
    SMGErrorType.h \
    FFmpeg.h

unix {
    target.path = /usr/lib
    INSTALLS += target
}

INCLUDEPATH +=  $$PWD/../3rdParty/include
INCLUDEPATH +=  $$PWD/../3rdParty/include/sdl2.0.5

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../3rdParty/lib/SDL -lSDL2
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../3rdParty/lib/SDL -lSDL2
else:unix:!macx: LIBS += -L$$PWD/../3rdParty/lib/SDL -lSDL2

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lavcodec
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lavcodec
else:unix:!macx: LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lavcodec

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lavdevice
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lavdevice
else:unix:!macx: LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lavdevice

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lavfilter
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lavfilter
else:unix:!macx: LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lavfilter

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lavformat
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lavformat
else:unix:!macx: LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lavformat

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lavutil
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lavutil
else:unix:!macx: LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lavutil

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lswresample
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lswresample
else:unix:!macx: LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lswresample

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lswscale
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lswscale
else:unix:!macx: LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lswscale

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lpostproc
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lpostproc
else:unix:!macx: LIBS += -L$$PWD/../3rdParty/lib/FFmpeg -lpostproc
