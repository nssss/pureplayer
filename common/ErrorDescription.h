#pragma once

#include "CommDef.h"

namespace smg_comm
{
	class ErrDescLoader
	{
	public:
		static void load_logic_err_description(IntStrMap& error_code_desc_map);
	};
}
