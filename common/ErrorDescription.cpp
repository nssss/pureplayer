
#include "SMGErrorType.h"
#include "ErrorDescription.h"
using namespace smg_comm;

void ErrDescLoader::load_logic_err_description(IntStrMap& error_code_desc_map)
{
    error_code_desc_map[eSHErr_logic_timeout]                           = "超时...";
    error_code_desc_map[eSHErr_logic_null_pointer]                      = "空指针...";
    error_code_desc_map[eSHErr_logic_arguments_invalid]                 = "参数无效...";
    error_code_desc_map[eSHErr_logic_memory_alloc_failed]               = "分配内存失败...";
    error_code_desc_map[eSHErr_logic_get_proc_address_failed]           = "显式加载DLL函数失败...";
    error_code_desc_map[eSHErr_logic_seek_media_param_invalid]          = "快进/退的目标值超出范围...";
    error_code_desc_map[eSHErr_logic_stream_url_not_found]              = "未查找到设备对应播放流的url...";
    error_code_desc_map[eSHErr_logic_device_hwnd_not_found]             = "未查找到设备对应的窗口句柄...";
    error_code_desc_map[eSHErr_logic_output_stream_url_not_found]       = "未查找到输出媒体流的url...";

    error_code_desc_map[eSHErr_logic_media_meta_not_set]                = "未设置媒体元信息...";
    error_code_desc_map[eSHErr_logic_media_meta_not_found]              = "未查找到媒体元信息...";
    error_code_desc_map[eSHErr_logic_media_not_support]                 = "不支持的媒体类型...";
    error_code_desc_map[eSHErr_logic_specific_media_not_found]          = "未查找到媒体类型信息...";
    error_code_desc_map[eSHErr_logic_state_machine_thread]              = "解码状态线程异常...";
																        
    error_code_desc_map[eSHErr_logic_method_not_implement]              = "方法尚未实现...";
    error_code_desc_map[eSHErr_logic_sdl_displayer_not_found]           = "未查找到匹配的SDL播放器...";
    error_code_desc_map[eSHErr_logic_decode_handler_not_found]          = "未查找到解码处理器...";
    error_code_desc_map[eSHErr_logic_encode_handler_not_found]          = "未查找到编码处理器...";
																        
    error_code_desc_map[eSHErr_logic_username_not_found]                = "用户名不存在...";
    error_code_desc_map[eSHErr_logic_userpassword_not_match]            = "密码不匹配...";
    error_code_desc_map[eSHErr_logic_user_already_encrolled]            = "用户已注册...";
																        
    error_code_desc_map[eSHErr_logic_command_invalid]                   = "控制命令无效...";
    error_code_desc_map[eSHErr_logic_task_response_invalid]             = "响应数据无效...";
    error_code_desc_map[eSHErr_logic_callback_not_set]                  = "回调函数未设置...";
    error_code_desc_map[eSHErr_logic_task_uuid_not_set]                 = "任务ID为空...";

}
