
#include "SDL.h"
#include "FFmpeg.h"
#include "ErrorDescMgr.h"
#include "ErrorDescription.h"

extern "C"{
//#include "error.h"
}

using namespace smg_comm;

int ErrorDescMgr::_last_out_err_code;
ErrorDescMgr::ErrorDescMgr()
{
	init_err_description_info();
}

ErrorDescMgr::~ErrorDescMgr()
{

}

ErrorDescMgr* ErrorDescMgr::instance()
{
	static ErrorDescMgr mgr;
	return &mgr;
}

const char* ErrorDescMgr::query_last_error_description()
{
	return query_error_description(_last_out_err_code);
}

const char* ErrorDescMgr::query_error_description(int out_err_code)
{
	const char* err_desc = nullptr;
	if (out_err_code < 0){
		return err_desc;
	}

	IntMap::const_iterator it = _err_code_map.find(out_err_code);
	if (it == _err_code_map.cend()){
		return err_desc;
	}

	int in_err_code = it->second;
	bool b_return_err_code = (out_err_code & 0x1000) == 0x1000;
	EBaseErrType base_type = (EBaseErrType)(out_err_code & 0x70000000);

	switch (base_type){
	case eBaseErr_ffmpeg:
	case eBaseErr_sdl:
	case eBaseErr_dhsdk:
	case eBaseErr_hksdk:
	case eBaseErr_database:{
		err_desc = query_internal_error_description(base_type, in_err_code);
		break;
	}

	case eBaseErr_logic:{
		err_desc = query_internal_error_description(in_err_code);
		break;
	}

	default:
		break;
	}

	return err_desc;
}

int ErrorDescMgr::query_internal_error_code(int out_err_code)
{
	IntMap::const_iterator it = _err_code_map.find(out_err_code);
	if (it == _err_code_map.cend()){
		return 0;
	}

	return it->second;
}

int ErrorDescMgr::record_error_code(int err_code)
{
	_err_code_map[err_code] = err_code;
	_last_out_err_code = err_code;
	return err_code;
}

int ErrorDescMgr::record_error_code(int out_err_code, int in_err_code)
{
	_err_code_map[out_err_code] = in_err_code;
	_last_out_err_code = out_err_code;
	return out_err_code;
}

int ErrorDescMgr::record_error_code(int err_code, const std::string& err_msg)
{
	_err_code_map[err_code] = err_code;
	_err_desc_map[err_code] = err_msg;
	_last_out_err_code = err_code;
	return err_code;
}

const char* ErrorDescMgr::query_internal_error_description(int in_err_code)
{
	std::unique_lock<std::mutex> lck(_err_desc_mtx);

	IntStrMap::const_iterator it = _err_desc_map.find(in_err_code);
	if (it == _err_desc_map.cend()){
		return nullptr;
	}

	return it->second.c_str();
}

const char* ErrorDescMgr::query_internal_error_description(EBaseErrType type, int in_err_code)
{
	char err[1024] = { 0 };

	switch (type){
	case eBaseErr_ffmpeg:{
		av_make_error_string(err, 1024, in_err_code);
		break;
	}
	case eBaseErr_sdl:{
		strcpy_s(err, SDL_GetError());
		break;
	}
	case eBaseErr_dhsdk:{
		strcpy_s(err, _dhsdk_err_desc_map[in_err_code].c_str());
		break;
	}
	case eBaseErr_hksdk:{
		strcpy_s(err, _hksdk_err_desc_map[in_err_code].c_str());
		break;
	}
	case eBaseErr_database:{
		break;
	}
	default:
		break;
	}

	std::unique_lock<std::mutex> lck(_err_desc_mtx);
	_err_desc_map[in_err_code] = err;

	return _err_desc_map[in_err_code].c_str();
}

void ErrorDescMgr::init_err_description_info()
{
	ErrDescLoader::load_logic_err_description(_err_desc_map);
}
