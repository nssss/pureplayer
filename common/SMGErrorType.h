
#pragma once

enum EBaseErrType
{
	eBaseErr_undef     = -1,
	eBaseErr_ffmpeg    = 0x10000000,
	eBaseErr_sdl       = 0x20000000,
	eBaseErr_dhsdk     = 0x30000000,
	eBaseErr_hksdk     = 0x40000000,
	eBaseErr_database  = 0x50000000,
	eBaseErr_network   = 0x60000000,
	eBaseErr_logic     = 0x70000000,
};

#define FFMPEG_ERROR(x)     (eBaseErr_ffmpeg | x)
#define SDL_ERROR(x)        (eBaseErr_sdl | x)
#define DHSDK_ERROR(x)      (eBaseErr_dhsdk | x)
#define HKSDK_ERROR(x)      (eBaseErr_hksdk | x)
#define DATABASE_ERROR(x)   (eBaseErr_database | x)
#define NETWORK_ERROR(x)    (eBaseErr_network | x)
#define LOGIC_ERROR(x)      (eBaseErr_logic | x)


enum EStreamHandlerErrType
{
	eSHErr_undef    = -1,
	eSHErr_success  = 0,
	eSHErr_continue = 1,

	//ffmpeg
	//FFmpeg错误,返回错误码
	//输入流
	eSHErr_ffmpeg_avformat_open_input                = FFMPEG_ERROR(0x1001),
	eSHErr_ffmpeg_avformat_find_stream_info          = FFMPEG_ERROR(0x1002),
													   					  
	//探查                                             					
	eSHErr_ffmpeg_av_probe_input_buffer              = FFMPEG_ERROR(0x1003),
													   					  
	//解码                                             				
	eSHErr_ffmpeg_av_read_frame                      = FFMPEG_ERROR(0x1004),
	eSHErr_ffmpeg_avcodec_send_packet                = FFMPEG_ERROR(0x1005),
	eSHErr_ffmpeg_avcodec_receive_frame              = FFMPEG_ERROR(0x1006),
													   					  
	//编码                                             					
	eSHErr_ffmpeg_avcodec_send_frame                 = FFMPEG_ERROR(0x1007),
	eSHErr_ffmpeg_avcodec_receive_packet             = FFMPEG_ERROR(0x1008),
													   					  
	//过滤器                                           				   
	eSHErr_ffmpeg_av_buffersrc_add_frame_flags       = FFMPEG_ERROR(0x1010),
	eSHErr_ffmpeg_av_buffersink_get_frame            = FFMPEG_ERROR(0x1011),
	eSHErr_ffmpeg_avfilter_graph_create_filter       = FFMPEG_ERROR(0x1012),
	eSHErr_ffmpeg_avfilter_graph_parse_ptr           = FFMPEG_ERROR(0x1013),
	eSHErr_ffmpeg_avfilter_graph_config              = FFMPEG_ERROR(0x1014),
													   					  
	//输出流                                           				   
	eSHErr_ffmpeg_avformat_alloc_output_context      = FFMPEG_ERROR(0x1020),
	eSHErr_ffmpeg_avio_open                          = FFMPEG_ERROR(0x1021),
	eSHErr_ffmpeg_avio_closep                        = FFMPEG_ERROR(0x1022),
	eSHErr_ffmpeg_avformat_write_header              = FFMPEG_ERROR(0x1023),
	eSHErr_ffmpeg_av_interleaved_write_frame         = FFMPEG_ERROR(0x1024),
	eSHErr_ffmpeg_av_write_trailer                   = FFMPEG_ERROR(0x1025),
													   					  
	//播放视频                                          				  
	eSHErr_ffmpeg_av_seek_frame                      = FFMPEG_ERROR(0x1030),
	eSHErr_ffmpeg_av_image_fill_arrays               = FFMPEG_ERROR(0x1031),
	eSHErr_ffmpeg_sws_scale                          = FFMPEG_ERROR(0x1032),
													   					  
	//公共                                             					
	eSHErr_ffmpeg_avformat_network_init              = FFMPEG_ERROR(0x1080),
	eSHErr_ffmpeg_avcodec_parameters_copy            = FFMPEG_ERROR(0x1081),
	eSHErr_ffmpeg_avcodec_parameters_from_context    = FFMPEG_ERROR(0x1082),
	eSHErr_ffmpeg_avcodec_parameters_to_context      = FFMPEG_ERROR(0x1083),
	eSHErr_ffmpeg_avcodec_open                       = FFMPEG_ERROR(0x1084),
	eSHErr_ffmpeg_av_frame_copy_props                = FFMPEG_ERROR(0x1085),
	eSHErr_ffmpeg_av_dict_set                        = FFMPEG_ERROR(0x1086),
	eSHErr_ffmpeg_av_opt_set                         = FFMPEG_ERROR(0x1087),
	eSHErr_ffmpeg_av_opt_set_bin                     = FFMPEG_ERROR(0x1088),
	eSHErr_ffmpeg_av_strdup                          = FFMPEG_ERROR(0x1089),
													   					  
	//FFmpeg错误,不返回错误码              			   					  
	//内存流  															  
	eSHErr_ffmpeg_avio_alloc_context                 = FFMPEG_ERROR(0x0001),
													   					  
	//过滤器                                           				   
	eSHErr_ffmpeg_avfilter_get_by_name               = FFMPEG_ERROR(0x0010),
	eSHErr_ffmpeg_avfilter_graph_alloc               = FFMPEG_ERROR(0x0011),
	eSHErr_ffmpeg_avfilter_inout_alloc               = FFMPEG_ERROR(0x0012),
													   					  
	//视频                                             					
	eSHErr_ffmpeg_av_frame_alloc                     = FFMPEG_ERROR(0x0020),
	eSHErr_ffmpeg_av_image_get_buffer_size           = FFMPEG_ERROR(0x0021),
	eSHErr_ffmpeg_av_malloc                          = FFMPEG_ERROR(0x0022),
	eSHErr_ffmpeg_sws_getContext                     = FFMPEG_ERROR(0x0023),
													   					  
	//音频                                             					
	eSHErr_ffmpeg_swr_alloc_set_opts                 = FFMPEG_ERROR(0x0030),
	eSHErr_ffmpeg_swr_init                           = FFMPEG_ERROR(0x0031),
	eSHErr_ffmpeg_swr_get_delay                      = FFMPEG_ERROR(0x0032),
	eSHErr_ffmpeg_swr_convert                        = FFMPEG_ERROR(0x0033),
	eSHErr_ffmpeg_av_get_default_channel_layout      = FFMPEG_ERROR(0x0040),
	eSHErr_ffmpeg_av_get_channel_layout_nb_channels  = FFMPEG_ERROR(0x0041),
	eSHErr_ffmpeg_av_get_bytes_per_sample            = FFMPEG_ERROR(0x0042),
	eSHErr_ffmpeg_av_get_sample_fmt_name             = FFMPEG_ERROR(0x0043),
													   					  
	//公共                                                   				
	eSHErr_ffmpeg_avformat_alloc_context             = FFMPEG_ERROR(0x0080),
	eSHErr_ffmpeg_avformat_new_stream                = FFMPEG_ERROR(0x0081),
	eSHErr_ffmpeg_avcodec_alloc_context              = FFMPEG_ERROR(0x0082),
	eSHErr_ffmpeg_avcodec_find_decoder               = FFMPEG_ERROR(0x0083),
	eSHErr_ffmpeg_avcodec_find_encoder               = FFMPEG_ERROR(0x0084),

	//sdl
	//SDL错误,返回错误码      
	eSHErr_sdl_init                                  = SDL_ERROR(0x1001),
													 
	//视频											 
	eSHErr_sdl_update_texture                        = SDL_ERROR(0x1002),
	eSHErr_sdl_render_clear                          = SDL_ERROR(0x1003),
	eSHErr_sdl_render_copy                           = SDL_ERROR(0x1004),
													   
	//音频											 
	eSHErr_sdl_open_audio                            = SDL_ERROR(0x1005),
													  
	//SDL错误,不返回错误码， SDL_GetError获取错误码	 
	eSHErr_sdl_create_window                         = SDL_ERROR(0x0001),
	eSHErr_sdl_create_window_from                    = SDL_ERROR(0x0002),
	eSHErr_sdl_create_renderer                       = SDL_ERROR(0x0003),
	eSHErr_sdl_create_texture                        = SDL_ERROR(0x0004),

	//sdk
	//SDK错误，返回错误码    
	//登录
	eSHErr_dhsdk_client_login                        = DHSDK_ERROR(0x1001),
													   
	//返回值为bool, CLIENT_GetLastError获取错误码	 
	eSHErr_dhsdk_client_init                         = DHSDK_ERROR(0x0001),
	//登出                                       		 
	eSHErr_dhsdk_client_stop_realplay_ex             = DHSDK_ERROR(0x0002),
	eSHErr_dhsdk_client_stop_playback                = DHSDK_ERROR(0x0003),
	eSHErr_dhsdk_client_stop_download                = DHSDK_ERROR(0x0004),
	eSHErr_dhsdk_client_logout                       = DHSDK_ERROR(0x0005),
	//实时预览                                     	   
	eSHErr_dhsdk_client_realplayex                   = DHSDK_ERROR(0x0010),
	eSHErr_dhsdk_client_set_realdata_callback_ex     = DHSDK_ERROR(0x0011),
	//按文件回放                                    	 
	eSHErr_dhsdk_client_set_device_mode              = DHSDK_ERROR(0x0012),
	eSHErr_dhsdk_client_playback_by_record_file_ex   = DHSDK_ERROR(0x0013),
	//查询录像文件                                     
	eSHErr_dhsdk_client_query_record_file            = DHSDK_ERROR(0x0014),
	//下载录像文件                                    
	eSHErr_dhsdk_client_download_by_record_file      = DHSDK_ERROR(0x0015),
	eSHErr_dhsdk_client_download_by_time             = DHSDK_ERROR(0x0016),

	//database
	eSHErr_db_connect_failure                        = DATABASE_ERROR(0x0001),
	eSHErr_db_disconn_failure                        = DATABASE_ERROR(0x0002),
	eSHErr_db_execute_sql_failure                    = DATABASE_ERROR(0x0003),

	//net
	eSHErr_network_http                              = NETWORK_ERROR(0x0001),

	//逻辑错误
	eSHErr_logic_timeout                             = LOGIC_ERROR(0x0001),
	eSHErr_logic_null_pointer                        = LOGIC_ERROR(0x0002),
	eSHErr_logic_arguments_invalid                   = LOGIC_ERROR(0x0003),
	eSHErr_logic_memory_alloc_failed                 = LOGIC_ERROR(0x0004),
	eSHErr_logic_get_proc_address_failed             = LOGIC_ERROR(0x0005),
	eSHErr_logic_seek_media_param_invalid            = LOGIC_ERROR(0x0006),

	eSHErr_logic_stream_url_not_found                = LOGIC_ERROR(0x0007),
	eSHErr_logic_device_hwnd_not_found               = LOGIC_ERROR(0x0008),
	eSHErr_logic_output_stream_url_not_found         = LOGIC_ERROR(0x0009),

	eSHErr_logic_media_meta_not_set                  = LOGIC_ERROR(0x0010),
	eSHErr_logic_media_meta_not_found                = LOGIC_ERROR(0x0011),
	eSHErr_logic_media_not_support                   = LOGIC_ERROR(0x0012),
	eSHErr_logic_specific_media_not_found            = LOGIC_ERROR(0x0013),
	eSHErr_logic_state_machine_thread                = LOGIC_ERROR(0x0014),

	eSHErr_logic_method_not_implement                = LOGIC_ERROR(0x0020),
	eSHErr_logic_sdl_displayer_not_found             = LOGIC_ERROR(0x0021),
	eSHErr_logic_decode_handler_not_found            = LOGIC_ERROR(0x0022),
	eSHErr_logic_encode_handler_not_found            = LOGIC_ERROR(0x0023),

	eSHErr_logic_username_not_found                  = LOGIC_ERROR(0x0030),
	eSHErr_logic_userpassword_not_match              = LOGIC_ERROR(0x0031),
	eSHErr_logic_user_already_encrolled              = LOGIC_ERROR(0x0032),

	eSHErr_logic_command_invalid                     = LOGIC_ERROR(0x0040),
	eSHErr_logic_task_response_invalid               = LOGIC_ERROR(0x0041),
	eSHErr_logic_callback_not_set                    = LOGIC_ERROR(0x0042),
	eSHErr_logic_task_uuid_not_set                   = LOGIC_ERROR(0x0043),
};

