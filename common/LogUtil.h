#pragma once

#include "spdlog/spdlog.h"
#include "common_global.h"
#include "CommDef.h"

namespace smg_comm
{
    class COMMONSHARED_EXPORT LogUtil
	{
	private:
		LogUtil();
		virtual ~LogUtil();

	public:
		static LogUtil* instance();

	public:
		//返回旧日志级别
		spdlog::level::level_enum change_log_level(spdlog::level::level_enum level);
		bool config_log(ELoggerType log_type, const std::string& store_dir, spdlog::level::level_enum level);

	public:
		template <typename T>
		void log(spdlog::level::level_enum level, const T& msg)
		{
			switch (_log_type){
			case eLog_file: write_log(_file_spd_log, level, msg); break;
			case eLog_console: write_log(_console_spd_log, level, msg); break;
			case eLog_console_file: write_log(_file_spd_log, level, msg); write_log(_console_spd_log, level, msg); break;
			default: break;
			}
		}

		template <typename T, typename... Args>
		void log(spdlog::level::level_enum level, const char* fmt, const T &arg, const Args &... args)
		{
			switch (_log_type){
			case eLog_file: write_log(_file_spd_log, level, fmt, arg, std::forward<Args>(args)); break;
			case eLog_console: write_log(_console_spd_log, level, fmt, arg, std::forward<Args>(args)); break;
			case eLog_console_file: write_log(_file_spd_log, level, fmt, arg, std::forward<Args>(args)); write_log(_console_spd_log, level, fmt, arg, std::forward<Args>(args)); break;
			default: break;
			}
		}

	private:
		typedef std::shared_ptr<spdlog::logger> SpdLog;

	private:
		template <typename T>
		void write_log(SpdLog logger, spdlog::level::level_enum level, const T& msg)
		{
			if (logger == nullptr){
				return;
			}

			switch (level){
			case level_enum::trace: logger->trace(msg); break;
			case level_enum::debug:logger->debug(msg); break;
			case level_enum::info: logger->info(msg); break;
			case level_enum::warn: logger->warn(msg); break;
			case level_enum::err:logger->error(msg); break;
			case level_enum::critical: logger->critical(msg); break;
			default: break;
			}
		}

		template <typename T, typename... Args>
		void write_log(SpdLog logger, spdlog::level::level_enum level, const char* fmt, const T &arg, const Args &... args)
		{
			if (logger == nullptr){
				return;
			}

			switch (level){
			case level_enum::trace: logger->trace(fmt, arg, std::forward<Args>(args)); break;
			case level_enum::debug:logger->debug(fmt, arg, std::forward<Args>(args)); break;
			case level_enum::info: logger->info(fmt, arg, std::forward<Args>(args)); break;
			case level_enum::warn: logger->warn(fmt, arg, std::forward<Args>(args)); break;
			case level_enum::err:logger->error(fmt, arg, std::forward<Args>(args)); break;
			case level_enum::critical: logger->critical(fmt, arg, std::forward<Args>(args)); break;
			default: break;
			}
		}

	private:
		ELoggerType _log_type;
		std::string _log_store_dir;
		spdlog::level::level_enum _log_level;

		SpdLog _file_spd_log;
		SpdLog _console_spd_log;
	};

#define LOG_DEBUG_FORMAT "%s(#%d)-%s: %s"
#define LOG_DEBUG_PARAMS __FILE__,__LINE__,__FUNCTION__

#define Log(level, msg) \
	char dp[1024] = {0}; \
	sprintf_s(dp, 1024, LOG_DEBUG_FORMAT, LOG_DEBUG_PARAMS, msg); \
	LogUtil::instance()->log<std::string>(level, dp);
}


