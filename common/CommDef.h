
#pragma once

#include <string>
#include <vector>
#include <mutex>
#include <thread>
#include <functional>
#include <unordered_set>
#include <unordered_map>
#include <condition_variable>

#define MAX_URL_LEN 256

typedef std::vector<std::string> StrVec;
typedef std::vector<StrVec> StrVVec;

typedef std::unordered_map<int, int> IntMap;
typedef std::unordered_map<int, std::string> IntStrMap;
typedef std::unordered_map<std::string, std::string> StrMap;
typedef std::unordered_map<std::string, void*> StrPtrMap;
typedef std::unordered_set<std::string> StrSet;

enum ELoggerType{
	eLog_undef        = -1,
	eLog_file         = 1,
	eLog_console      = 2,
	eLog_console_file = 4,
};

enum EDECodeOperType{
	eDECOper_start    = 0x01,
	eDECOper_keep     = 0x02,
	eDECOper_pause    = 0x04,
	eDECOper_exit     = 0x08,
};

enum EMediaStreamType{
	eMediaStream_undef    = -1,
	eMediaStream_realtime = 0,
	eMediaStream_playback = 1,
};

enum EStreamActionType{
    eStreamAction_undef       = -1,
    eStreamAction_push_stream = 0,
    eStreamAction_pull_stream = 1,
};
