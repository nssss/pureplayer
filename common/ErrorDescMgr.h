#pragma once

#include "CommDef.h"
#include "SMGErrorType.h"
#include "common_global.h"

namespace smg_comm
{
    class COMMONSHARED_EXPORT ErrorDescMgr
	{
	private:
		ErrorDescMgr();
		~ErrorDescMgr();

	public:
		static ErrorDescMgr* instance();

	public:
		const char* query_last_error_description();
		const char* query_error_description(int out_err_code);
		int query_internal_error_code(int out_err_code);

		int record_error_code(int err_code);
		int record_error_code(int out_err_code, int in_err_code);
		int record_error_code(int err_code, const std::string& err_msg);

	private:
		void init_err_description_info();

	private:
		const char* query_internal_error_description(int in_err_code);                      //无错误码返回
		const char* query_internal_error_description(EBaseErrType type, int in_err_code);   //有错误码返回

	private:
		IntMap _err_code_map;
		IntStrMap _err_desc_map;
		std::mutex _err_desc_mtx;
		static int _last_out_err_code;

	private:
		IntStrMap _dhsdk_err_desc_map;
		IntStrMap _hksdk_err_desc_map;
	};
}



