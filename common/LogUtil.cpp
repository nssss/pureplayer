#include "LogUtil.h"
using namespace smg_comm;

LogUtil::LogUtil()
{
	
}

LogUtil::~LogUtil()
{

}

LogUtil* LogUtil::instance()
{
	static LogUtil util;
	return &util;
}

bool LogUtil::config_log(ELoggerType log_type, const std::string& store_dir, spdlog::level::level_enum level)
{
	if (log_type == eLog_undef || store_dir.empty()){
		return false;
	}

	_log_level = level;
	_log_type = log_type;
	_log_store_dir = store_dir;

	switch (_log_type){
	case eLog_file: {
		_file_spd_log = spdlog::rotating_logger_mt("file_logger", store_dir, 1024 * 1024 * 5, 3);
		_file_spd_log->set_level(_log_level);
		break;
	}
	case eLog_console: {
		_console_spd_log = spdlog::stdout_color_mt("console_logger");
		_console_spd_log->set_level(_log_level);
		break;
	}
	case eLog_console_file: {
		_console_spd_log = spdlog::stdout_color_mt("console_logger");
		_console_spd_log->set_level(_log_level);

		_file_spd_log = spdlog::rotating_logger_mt("file_logger", store_dir, 1024 * 1024 * 5, 3);
		_console_spd_log->set_level(_log_level);
		break;
	}
	default: break;
	}

	return true;
}

spdlog::level::level_enum LogUtil::change_log_level(spdlog::level::level_enum level)
{
	if (level == _log_level){
		return _log_level;
	}

	switch (_log_type){
	case eLog_file: _file_spd_log->set_level(level); break;
	case eLog_console: _console_spd_log->set_level(level); break;
	case eLog_console_file: _file_spd_log->set_level(level); _console_spd_log->set_level(level); break;
	default: break;
	}

	spdlog::level::level_enum old_level = _log_level;
	_log_level = level;

	return old_level;
}

