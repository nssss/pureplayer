#ifndef SDLDISP_GLOBAL_H
#define SDLDISP_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(SDLDISP_LIBRARY)
#  define SDLDISPSHARED_EXPORT Q_DECL_EXPORT
#else
#  define SDLDISPSHARED_EXPORT Q_DECL_IMPORT
#endif

#endif // SDLDISP_GLOBAL_H
