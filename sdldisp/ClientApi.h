
#pragma once

#include <cinttypes>

#ifndef DLLEXPORT
#define DLL_API _declspec(dllexport)
#else
#define DLL_API _declspec(dllimport)
#endif

//订阅播放事件，当触发事件时，自动推送媒体流播放
enum EPlayEventType{
	ePlayEvent_undef   = -1,
	ePlayEvent_monitor = 0,
};

enum EPlayOperType{
	ePlayOper_undef    = -1,
	ePlayOper_start    = 0x01,       //开始播放
	ePlayOper_keep     = 0x02,       //继续播放
	ePlayOper_pause    = 0x04,       //暂停播放
	ePlayOper_exit     = 0x08,       //退出播放
	ePlayOper_realtime = 0x10,       //实时播放
	ePlayOper_playback = 0x20,       //回放
};

typedef void (*ResourceCallback)(const char*);

#ifdef __cplusplus
extern "C" {
#endif

//初始化、清理
int DLL_API stream_media_handler_init();
int DLL_API stream_media_handler_exit();

//登录、登出
int DLL_API stream_media_handler_login(const char* usr, const char* pwd);
int DLL_API stream_media_handler_logout(const char* usr);

//绑定播放窗口，允许重复绑定，应用于画面在不同窗口轮巡的情形
int DLL_API stream_media_handler_bind_play_window(const char* dev_id, void* hwnd);

//更新播放窗口，应用于窗口大小动态改变的情形
int DLL_API stream_media_handler_update_play_window(const char* dev_id);

//订阅播放事件
int DLL_API stream_media_handler_subscribe_play_event(EPlayEventType event, void* hwnd);

//执行播放操作
int DLL_API stream_media_handler_execute_play_operation(const char* dev_id, int oper);

//调整音量
int DLL_API stream_media_handler_query_media_volume(const char* dev_id, int* vol);
int DLL_API stream_media_handler_adjust_media_volume(const char* dev_id, int vol);

//查询、调整进度，单位ms
int DLL_API stream_media_handler_query_media_progress(const char* dev_id, int64_t* cur_ts);
int DLL_API stream_media_handler_adjust_media_progress(const char* dev_id, int64_t seek_ts);

//查询媒体时长，单位ms
int DLL_API stream_media_handler_query_media_duration(const char* dev_id, int64_t* duration);

//查询媒体资源列表，即当前可播放资源信息,设置资源信息更新回调
int DLL_API stream_media_handler_query_media_resource(char* resource, ResourceCallback callback);

//查询错误原因
DLL_API const char* stream_media_handler_query_error_description(int err_no);

#ifdef __cplusplus
}
#endif
