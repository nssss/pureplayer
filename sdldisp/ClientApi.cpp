
#include "ClientApi.h"
#include "ClientMgr.h"
#include "../BaseTypeDef/ErrorDescMgr.h"
using namespace smg_api;
using namespace smg_comm;

//初始化、清理
int stream_media_handler_init()
{
	return ClientMgr::instance()->initialize();
	return 0;
}

int stream_media_handler_exit()
{
	return 0;
}

//登录、登出
int stream_media_handler_login(const char* usr, const char* pwd)
{
	if (!usr || !pwd){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
		return 0;
	}

	return ClientMgr::instance()->login(usr, pwd);
}

int stream_media_handler_logout(const char* usr)
{
	return ClientMgr::instance()->logout(usr);
}

//绑定播放窗口，允许重复绑定，应用于画面在不同窗口轮巡的情形
int stream_media_handler_bind_play_window(const char* dev_id, void* hwnd)
{
	return ClientMgr::instance()->bind_play_window(dev_id, hwnd);
}

//更新播放窗口，应用于窗口大小动态改变的情形
int stream_media_handler_update_play_window(const char* dev_id)
{
	return ClientMgr::instance()->update_play_window(dev_id);
}

//订阅播放事件
int stream_media_handler_subscribe_play_event(EPlayEventType event_type, void* hwnd)
{
	return ClientMgr::instance()->subscribe_play_event(event_type, hwnd);
}

//执行播放操作
int stream_media_handler_execute_play_operation(const char* dev_id, int oper)
{
	int ret = eSHErr_success;
	int oper_type = oper & 0x0f;
	int stream_type = oper & 0x70;

	if (oper_type == eDECOper_start){
		if ((ret = ClientMgr::instance()->ready_play(dev_id, stream_type == ePlayOper_realtime)) != eSHErr_success){
			return ret;
		}
	}

	return ClientMgr::instance()->execute_play_operation(dev_id, oper_type);
}

//查询音量
int stream_media_handler_query_media_volume(const char* dev_id, int* vol)
{
	return ClientMgr::instance()->query_media_volume(dev_id, vol);
}

//调整音量
int stream_media_handler_adjust_media_volume(const char* dev_id, int vol)
{
	return ClientMgr::instance()->adjust_media_volume(dev_id, vol);
}

//查询、调整进度
int stream_media_handler_query_media_progress(const char* dev_id, int64_t* cur_ts)
{
	return ClientMgr::instance()->query_media_progress(dev_id, cur_ts);
}

int stream_media_handler_adjust_media_progress(const char* dev_id, int64_t seek_ts)
{
	return ClientMgr::instance()->adjust_media_progress(dev_id, seek_ts);
}

//查询媒体播放持续时长
int stream_media_handler_query_media_duration(const char* dev_id, int64_t* duration)
{
	return ClientMgr::instance()->query_media_duration(dev_id, duration);
}

//查询媒体资源列表，即当前可播放资源信息, 设置资源信息更新回调
int stream_media_handler_query_media_resource(char* resource, ResourceCallback callback)
{
	ClientMgr::instance()->register_media_resource_callback(callback);
	return ClientMgr::instance()->query_media_resource(resource);
}

//查询错误原因
const char* stream_media_handler_query_error_description(int err_no)
{
	return ClientMgr::instance()->query_error_description(err_no);
}

