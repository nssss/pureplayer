
#include "SDLDisplayerManager.h"
#include "../common/ErrorDescMgr.h"
using namespace smg_core;
using namespace smg_comm;
using namespace smg_player;

SDLDisplayerManager::SDLDisplayerManager()
{
	_media_nanager = MediaManager::instance();
}

SDLDisplayerManager::~SDLDisplayerManager()
{
	
}

SDLDisplayerManager* SDLDisplayerManager::instance()
{
	static SDLDisplayerManager mgr;
	return &mgr;
}

int SDLDisplayerManager::update_displayer(const char* url)
{
	if (!url){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
	}

	SDLDisplayer* disp = find_sdl_displayer(url);
	if (!disp){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_sdl_displayer_not_found);
	}

	disp->set_reset_displayer_flag();

	return eSHErr_success;
}

int SDLDisplayerManager::config_displayer(const char* url, void* hwnd, EMediaStreamType type)
{
	if (!url || !hwnd || type == eMediaStream_undef){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
	}

	int ret = 0;
    if ((ret = _media_nanager->open_media(url, nullptr, type, eStreamAction_pull_stream)) != eSHErr_success){
		return ret;
	}

	MediaMeta* meta = _media_nanager->find_media_meta(url);
	if (!meta){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_media_meta_not_found);
	}

	SDLDisplayer* sdl_displayer = new SDLDisplayer();
	if (!sdl_displayer){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_memory_alloc_failed);
	}

	StreamInfo* video_info = meta->find_stream_info(AVMEDIA_TYPE_VIDEO);
	StreamInfo* audio_info = meta->find_stream_info(AVMEDIA_TYPE_AUDIO);
	if (!video_info && !audio_info){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_specific_media_not_found);
	}

	MediaConfParam media_conf_param;

	if (video_info){
		media_conf_param.v_hwnd = hwnd;
		media_conf_param.b_v_enable = true;
		media_conf_param.pix_format = AV_PIX_FMT_YUV420P;
		media_conf_param.orig_width = video_info->_codec_ctx[eInOut_in]->width;
		media_conf_param.orig_height = video_info->_codec_ctx[eInOut_in]->height;
		video_info->_frm_callback = std::bind(&SDLDisplayer::display_media_callback, sdl_displayer, std::placeholders::_1, std::placeholders::_2);
	}

	if (audio_info){
		media_conf_param.b_a_enable = true;
		media_conf_param.audio_buf_size = 1024;
		media_conf_param.audio_format = AUDIO_S16SYS;
		media_conf_param.sample_rate = audio_info->_codec_ctx[eInOut_in]->sample_rate;
		media_conf_param.audio_channel = audio_info->_codec_ctx[eInOut_in]->channels;
		audio_info->_frm_callback = std::bind(&SDLDisplayer::display_media_callback, sdl_displayer, std::placeholders::_1, std::placeholders::_2);
	}

	if ((ret = sdl_displayer->config_displayer(meta, media_conf_param)) == eSHErr_success){
		std::unique_lock<std::mutex> lck(_sdl_disp_map_mtx);
		_sdl_displayer_map[url] = sdl_displayer;
	}

	return ret;
}

int SDLDisplayerManager::register_play_event_callback(const char* url, EventCallback event_callback)
{
	return 0;
}

int SDLDisplayerManager::operate_display(const char* url, EDECodeOperType oper)
{
	if (!url){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
	}

	SDLDisplayer* disp = find_sdl_displayer(url);
	if (!disp){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_sdl_displayer_not_found);
	}

	int ret = 0;
	if (oper == eDECOper_start){
		if ((ret = _media_nanager->execute_decode(url)) != eSHErr_success){
			return ret;
		}

		return disp->start_displayer();
	}
	else if (oper == eDECOper_exit){
		if ((ret = _media_nanager->operate_decode(url, oper)) != eSHErr_success); {
			return ret;
		}

		if ((ret = disp->exit_displayer()) != eSHErr_success){
			return ret;
		}

		remove_sdl_displayer(url);
		return true;
	}

	return _media_nanager->operate_decode(url, oper);
}

int SDLDisplayerManager::query_media_volume(const char* url, int* vol)
{
	if (!url){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
	}

	SDLDisplayer* disp = find_sdl_displayer(url);
	if (!disp){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_sdl_displayer_not_found);
	}

	return disp->query_media_volume(*vol);
}

int SDLDisplayerManager::adjust_media_volume(const char* url, int vol)
{
	if (!url){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
	}

	SDLDisplayer* disp = find_sdl_displayer(url);
	if (!disp){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_sdl_displayer_not_found);
	}

	return disp->adjust_media_volume(vol);
}

int SDLDisplayerManager::query_media_progress(const char* url, int64_t* cur_ts)
{
	return _media_nanager->query_media_progress(url, *cur_ts);
}

int SDLDisplayerManager::adjust_media_progress(const char* url, int64_t seek_ts)
{
	return _media_nanager->adjust_media_progress(url, seek_ts);
}

int SDLDisplayerManager::query_media_duration(const char* url, int64_t* duration)
{
	return _media_nanager->query_media_duration(url, *duration);
}

int SDLDisplayerManager::query_media_resource(const StrVec& res_tbl)
{
	return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_method_not_implement);
}

const char* SDLDisplayerManager::query_error_description(int err_code)
{
	return ErrorDescMgr::instance()->query_error_description(err_code);
}

SDLDisplayer* SDLDisplayerManager::find_sdl_displayer(const char* url)
{
	std::unique_lock<std::mutex> lck(_sdl_disp_map_mtx);

	SDLDisplayerMap::iterator it = _sdl_displayer_map.find(url);
	if (it == _sdl_displayer_map.end()){
		return nullptr;
	}

	return it->second;
}

bool SDLDisplayerManager::remove_sdl_displayer(const char* url)
{
	bool ret = true;
	std::unique_lock<std::mutex> lck(_sdl_disp_map_mtx);

	SDLDisplayerMap::iterator it = _sdl_displayer_map.find(url);
	ret = it != _sdl_displayer_map.end();

	if (ret){
		SDLDisplayer* displayer = it->second;
		_sdl_displayer_map.erase(it);
		delete displayer;
	}

	return ret;
}
