#pragma once

#include <mutex>
#include "SDLDisplayer.h"
#include "../decode/MediaManager.h"
#include "sdldisp_global.h"

namespace smg_player
{
    class SDLDISPSHARED_EXPORT SDLDisplayerManager
	{
	private:
		SDLDisplayerManager();
		virtual ~SDLDisplayerManager();

	public:
		static SDLDisplayerManager* instance();

	public:
		int update_displayer(const char* url);
		int config_displayer(const char* url, void* hwnd, EMediaStreamType type);

	public:
		int operate_display(const char* url, EDECodeOperType oper);

	public:
		//volume
		int query_media_volume(const char* url, int* vol);
		int adjust_media_volume(const char* url, int vol);

		//video
		int query_media_progress(const char* url, int64_t* cur_ts);
		int adjust_media_progress(const char* url, int64_t seek_ts);

	public:
		int query_media_duration(const char* url, int64_t* duration);
		int query_media_resource(const StrVec& res_tbl);

	public:
		int register_play_event_callback(const char* url, EventCallback event_callback);
		int update_media_resource_callback(const StrVec& res_tbl);

	public:
		const char* query_error_description(int err_code);

	private:
		bool remove_sdl_displayer(const char* url);
		SDLDisplayer* find_sdl_displayer(const char* url);

	private:
		std::mutex _sdl_disp_map_mtx;
		SDLDisplayerMap _sdl_displayer_map;
		smg_core::MediaManager* _media_nanager;
	};
}
