
#include "SDLDisplayer.h"
#include "../common/ErrorDescMgr.h"
using namespace smg_comm;
using namespace smg_player;

MediaConfParam::MediaConfParam()
{
	memset(this, 0, sizeof(MediaConfParam));
}

MediaConfParam::MediaConfParam(const MediaConfParam& param)
{
	*this = param;
}

MediaConfParam& MediaConfParam::operator = (const MediaConfParam& param)
{
	if (this == &param){
		return *this;
	}

	memcpy(this, &param, sizeof(param));

	return *this;
}

SDLDisplayer::SDLDisplayer()
{
	_b_quit = false;
	_b_reset_displayer = false;
	_media_meta = nullptr;

	memset(&_v_play_param, 0, sizeof(VideoPlayParam));
	memset(&_a_play_param, 0, sizeof(AudioPlayParam));
}

SDLDisplayer::~SDLDisplayer()
{

}

int SDLDisplayer::config_displayer(const smg_core::MediaMeta* meta, const MediaConfParam& param)
{
	if (!meta){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
	}

	int ret = eSHErr_success;
	_media_conf_param = param;
	_media_meta = const_cast<smg_core::MediaMeta*>(meta);

	if (SDL_Init(SDL_INIT_AUDIO | SDL_INIT_VIDEO | SDL_INIT_TIMER) != 0){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_sdl_init);
	}

	//audio
	if (param.b_a_enable){
		SDL_AudioSpec spec;
		SDL_memset(&spec, 0, sizeof(SDL_AudioSpec));

		spec.freq = param.sample_rate;               //采样率
		spec.format = param.audio_format;
		spec.channels = param.audio_channel;         //通道数
		spec.samples = param.audio_buf_size;         //缓冲区的大小

		spec.callback = audio_callback;              //音频设备数据回调函数
		spec.userdata = this;

		if (SDL_OpenAudio(&spec, nullptr) < 0) {
			//return ErrorDescMgr::instance()->record_error_code(eSHErr_sdl_open_audio);
		}

		_a_play_param.audio_volume = SDL_MIX_MAXVOLUME / 3;
	}

	//video
	if (param.b_v_enable){
		if (!(_v_play_param.sdl_window = SDL_CreateWindowFrom(param.v_hwnd))){
			return ErrorDescMgr::instance()->record_error_code(eSHErr_sdl_create_window_from);
		}

		if (!(_v_play_param.sdl_render = SDL_CreateRenderer(_v_play_param.sdl_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC))){
			return ErrorDescMgr::instance()->record_error_code(eSHErr_sdl_create_renderer);
		}

		if (!(_v_play_param.sdl_texture = SDL_CreateTexture(_v_play_param.sdl_render, SDL_PIXELFORMAT_IYUV, SDL_TEXTUREACCESS_STREAMING, param.orig_width, param.orig_height))){
			return ErrorDescMgr::instance()->record_error_code(eSHErr_sdl_create_texture);
		}

		ret = reset_swscontext(param.orig_width, param.orig_height);

		_v_play_param.sdl_rect.x = _v_play_param.sdl_rect.y = 0;
		SDL_GetWindowSize(_v_play_param.sdl_window, &_v_play_param.sdl_rect.w, &_v_play_param.sdl_rect.h);

		_media_conf_param.disp_width = _v_play_param.sdl_rect.w;
		_media_conf_param.disp_height = _v_play_param.sdl_rect.h;
	}

	return eSHErr_success;
}

int SDLDisplayer::reset_swscontext(int width, int height)
{
	if (_v_play_param.out_buf){
		av_free(_v_play_param.out_buf);
		_v_play_param.out_buf = nullptr;
	}

	if (_v_play_param.yuv_frm){
		av_frame_free(&_v_play_param.yuv_frm);
		_v_play_param.yuv_frm = nullptr;
	}

	if (_v_play_param.sws_ctx){
		sws_freeContext(_v_play_param.sws_ctx);
		_v_play_param.sws_ctx = nullptr;
	}

	int buf_size = av_image_get_buffer_size(AV_PIX_FMT_YUV420P, width, height, 1);
	if (!(_v_play_param.out_buf = (uint8_t*)av_malloc(sizeof(uint8_t) * buf_size))){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_memory_alloc_failed);
	}

	if (!(_v_play_param.yuv_frm = av_frame_alloc())){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_memory_alloc_failed);
	}

	int ret = eSHErr_success;
	if ((ret = av_image_fill_arrays(_v_play_param.yuv_frm->data, _v_play_param.yuv_frm->linesize, _v_play_param.out_buf, AV_PIX_FMT_YUV420P, width, height, 1)) < 0){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_av_image_fill_arrays, ret);
	}

	if (!(_v_play_param.sws_ctx = sws_getContext(_media_conf_param.orig_width, _media_conf_param.orig_height, (AVPixelFormat)_media_conf_param.pix_format, width, height, AV_PIX_FMT_YUV420P, SWS_BICUBIC, nullptr, nullptr, nullptr))){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_sws_getContext, eSHErr_logic_null_pointer);
	}

	return eSHErr_success;
}

int SDLDisplayer::start_displayer()
{
	if (!_media_meta){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_media_meta_not_set);
	}

	if (_media_conf_param.b_a_enable){
		SDL_PauseAudio(0);
	}

	return eSHErr_success;
}

int SDLDisplayer::exit_displayer()
{
	return clear_displayer();
}

void SDLDisplayer::set_reset_displayer_flag()
{
	_b_reset_displayer = true;
}

void SDLDisplayer::register_play_event_callback(EventCallback event_callback)
{

}

int SDLDisplayer::query_media_volume(int& vol)
{
	if (!_media_meta){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_media_meta_not_set);
	}

	vol = _a_play_param.audio_volume * 100 / SDL_MIX_MAXVOLUME;
	return eSHErr_success;
}

int SDLDisplayer::adjust_media_volume(int vol)
{
	if (!_media_meta){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_media_meta_not_set);
	}

	if (vol > 100){
		vol = 100;
	}

	_a_play_param.audio_volume = vol * SDL_MIX_MAXVOLUME / 100;
	return eSHErr_success;
}

int SDLDisplayer::reset_displayer()
{
	if (_v_play_param.sdl_texture){
		SDL_DestroyTexture(_v_play_param.sdl_texture);
	}

	if (_v_play_param.sdl_render){
		SDL_DestroyRenderer(_v_play_param.sdl_render);
	}

	if (!(_v_play_param.sdl_render = SDL_CreateRenderer(_v_play_param.sdl_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC))){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_sdl_create_renderer);
	}

	SDL_GetWindowSize(_v_play_param.sdl_window, &_media_conf_param.disp_width, &_media_conf_param.disp_height);
	_v_play_param.sdl_rect.w = _media_conf_param.disp_width;
	_v_play_param.sdl_rect.h = _media_conf_param.disp_height;

	if (!(_v_play_param.sdl_texture = SDL_CreateTexture(_v_play_param.sdl_render, SDL_PIXELFORMAT_IYUV, SDL_TEXTUREACCESS_STREAMING, _media_conf_param.disp_width, _media_conf_param.disp_height))){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_sdl_create_texture);
	}

	reset_swscontext(_media_conf_param.disp_width, _media_conf_param.disp_height);

	_b_reset_displayer = false;

	return eSHErr_success;
}

int SDLDisplayer::clear_displayer()
{
	_b_quit = true;

	if (_v_play_param.sdl_texture) {
		SDL_DestroyTexture(_v_play_param.sdl_texture);
		_v_play_param.sdl_texture = nullptr;
	}

	if (_v_play_param.sdl_render) {
		SDL_DestroyRenderer(_v_play_param.sdl_render);
		_v_play_param.sdl_render = nullptr;
	}

	if (_v_play_param.sdl_window) {
		SDL_DestroyWindow(_v_play_param.sdl_window);
		_v_play_param.sdl_window = nullptr;
	}

	if (_v_play_param.out_buf){
		av_free(_v_play_param.out_buf);
		_v_play_param.out_buf = nullptr;
	}

	if (_v_play_param.yuv_frm) {
		av_frame_free(&_v_play_param.yuv_frm);
		_v_play_param.yuv_frm = nullptr;
	}

	if (_v_play_param.sws_ctx) {
		sws_freeContext(_v_play_param.sws_ctx);
		_v_play_param.sws_ctx = nullptr;
	}

	SDL_CloseAudio();
	SDL_Quit();

	_b_quit = false;

	return eSHErr_success;
}

int SDLDisplayer::display_media_callback(const smg_core::StreamInfo* info, void* frame)
{
	int ret = eSHErr_success;
	switch (info->_media_type){
	case AVMEDIA_TYPE_VIDEO: ret = display_video(info, (AVFrame*)frame); break;
	case AVMEDIA_TYPE_AUDIO: ret = display_audio(info, (AVFrame*)frame); break;
	default: break;
	}

	return ret;
}

int SDLDisplayer::display_video(const smg_core::StreamInfo* info, const AVFrame* frame)
{
	if (!info || !frame){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
	}

	if (_b_reset_displayer){
		return reset_displayer();
	}

	int ret = eSHErr_success;
	if ((ret = sws_scale(_v_play_param.sws_ctx, frame->data, frame->linesize, 0, _media_conf_param.orig_height, _v_play_param.yuv_frm->data, _v_play_param.yuv_frm->linesize)) < 0){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_sws_scale, ret);
	}

	if ((ret = SDL_UpdateYUVTexture(_v_play_param.sdl_texture, nullptr,
		_v_play_param.yuv_frm->data[0],
		_v_play_param.yuv_frm->linesize[0],
		_v_play_param.yuv_frm->data[1],
		_v_play_param.yuv_frm->linesize[1],
		_v_play_param.yuv_frm->data[2],
		_v_play_param.yuv_frm->linesize[2])) < 0){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_sdl_update_texture, ret);
	}

	if ((ret = SDL_RenderClear(_v_play_param.sdl_render)) < 0){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_sdl_render_clear, ret);
	}

	if ((ret = SDL_RenderCopy(_v_play_param.sdl_render, _v_play_param.sdl_texture, nullptr, nullptr)) < 0){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_sdl_render_copy, ret);
	}

	SDL_RenderPresent(_v_play_param.sdl_render);
	
	return eSHErr_success;
}

int SDLDisplayer::display_audio(const smg_core::StreamInfo* info, const AVFrame* frame)
{
	int ret = 0;
	int buf_size = 0;
	if ((ret = resample_audio(const_cast<AVFrame*>(frame), _a_play_param.audio_buf, &buf_size)) != eSHErr_success){
		return ret;
	}

	while (_a_play_param.audio_len > 0){
		SDL_Delay(1);
	}

	_a_play_param.audio_len = buf_size;
	_a_play_param.audio_chunk = (uint8_t*)_a_play_param.audio_buf;
	_a_play_param.audio_index = _a_play_param.audio_chunk;

	return 0;
}

int SDLDisplayer::resample_audio(AVFrame* frm, uint8_t* out, int* audio_len)
{
	if (!frm || !out || !audio_len) {
		return ErrorDescMgr::instance()->record_error_code(eSHErr_logic_arguments_invalid);
	}

	// 设置通道数或channel_layout
	if (frm->channels > 0 && frm->channel_layout == 0) {
		frm->channel_layout = av_get_default_channel_layout(frm->channels);
	}
	else if (frm->channels == 0 && frm->channel_layout > 0) {
		frm->channels = av_get_channel_layout_nb_channels(frm->channel_layout);
	}

	AVSampleFormat dst_format = AV_SAMPLE_FMT_S16;                         //av_get_packed_sample_fmt((AVSampleFormat)frame->format);
	uint64_t dst_layout = av_get_default_channel_layout(frm->channels);

	// 设置转换参数
	SwrContext* swr_ctx = swr_alloc_set_opts(nullptr, 
											 dst_layout, 
											 dst_format,
											 frm->sample_rate, 
											 frm->channel_layout, 
											 (AVSampleFormat)frm->format,
											 frm->sample_rate, 
											 0, 
											 nullptr);

	if (!swr_ctx){
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_swr_alloc_set_opts, eSHErr_logic_memory_alloc_failed);
	}

	int ret = 0;
	if ((ret = swr_init(swr_ctx)) < 0) {
		return ErrorDescMgr::instance()->record_error_code(eSHErr_ffmpeg_swr_init, ret);
	}

	// 计算转换后的sample个数 a * b / c
	uint64_t delay_len = swr_get_delay(swr_ctx, frm->sample_rate) + frm->nb_samples;
	int dst_nb_samples = av_rescale_rnd(delay_len, frm->sample_rate, frm->sample_rate, AVRounding(1));

	// 转换，返回值为转换后的sample个数
	int nb = swr_convert(swr_ctx, &out, dst_nb_samples, (const uint8_t**)frm->data, frm->nb_samples);
	swr_free(&swr_ctx);

	*audio_len = frm->channels * nb * av_get_bytes_per_sample(dst_format);

	return eSHErr_success;
}

void SDLDisplayer::audio_callback(void* data, uint8_t* stream, int need_len)
{
	SDL_memset(stream, 0, need_len);
	SDLDisplayer* disp = (SDLDisplayer*)data;

	if (disp->_a_play_param.audio_len == 0){
		return;
	}

	if (need_len > disp->_a_play_param.audio_len){
		need_len = disp->_a_play_param.audio_len;
	}

	SDL_MixAudio(stream, disp->_a_play_param.audio_index, need_len, disp->_a_play_param.audio_volume);

	disp->_a_play_param.audio_index += need_len;
	disp->_a_play_param.audio_len -= need_len;
}

