#pragma once

#include <atomic>
#include <unordered_map>

#include "SDL.h"
#include "../decode/MediaMeta.h"
#include "../common/CommDef.h"

namespace smg_player
{
    #define SDL_AUDIO_SAMPLES 1024
    #define MAX_AUDIO_FRAME_SIZE 192000
    #define AUDIO_CHUNK_SIZE (MAX_AUDIO_FRAME_SIZE * 3 / 2)
    #define __STDC_CONSTANT_MACROS

	typedef std::function<int(int, void*)> EventCallback;

	struct MediaConfParam
	{
	public:
		MediaConfParam();
		MediaConfParam(const MediaConfParam& param);

	public:
		MediaConfParam& operator = (const MediaConfParam& param);

	public:
		//video
		bool b_v_enable;
		void* v_hwnd;
		int pix_format;
		int disp_width;
		int disp_height;
		int orig_width;
		int orig_height;

		//audio
		bool b_a_enable;
		int sample_rate;
		int audio_volume;
		int audio_format;
		int audio_channel;
		int audio_buf_size;
	};

	class SDLDisplayer
	{
	public:
		SDLDisplayer();
		virtual ~SDLDisplayer();

	public:
		int config_displayer(const smg_core::MediaMeta* meta, const MediaConfParam& param);
		int start_displayer();
		int exit_displayer();

		void set_reset_displayer_flag();

	public:
		int display_media_callback(const smg_core::StreamInfo* info, void* frame);
		void register_play_event_callback(EventCallback event_callback);

	public:
		int query_media_volume(int& vol);
		int adjust_media_volume(int vol);

	private:
		int reset_displayer();
		int clear_displayer();
		int reset_swscontext(int width, int height);

	public:
		int display_video(const smg_core::StreamInfo* info, const AVFrame* frame);
		int display_audio(const smg_core::StreamInfo* info, const AVFrame* frame);

	private:
		int resample_audio(AVFrame* frm, uint8_t* out, int* audio_len);
		static void audio_callback(void* data, uint8_t* stream, int need_len);
	
	private:
		struct VideoPlayParam{
			uint8_t* out_buf;
			AVFrame* yuv_frm;
			SwsContext* sws_ctx;
			SDL_Rect sdl_rect;
			SDL_Window* sdl_window;
			SDL_Renderer* sdl_render;
			SDL_Texture* sdl_texture;
		} _v_play_param;

		struct AudioPlayParam{
			int audio_len;
			int audio_volume;
			uint8_t* audio_index;
			uint8_t* audio_chunk;
			uint8_t audio_buf[AUDIO_CHUNK_SIZE];
		} _a_play_param;

	private:
		std::atomic<bool> _b_quit;
		std::atomic<bool> _b_reset_displayer;

	private:
		EventCallback _event_callback;
		MediaConfParam _media_conf_param;
		smg_core::MediaMeta* _media_meta;
	};

	typedef std::unordered_map<std::string, SDLDisplayer*> SDLDisplayerMap;
}



