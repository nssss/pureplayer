#ifndef PLAY_LIST_H
#define PLAY_LIST_H

#include <QFrame>

namespace Ui {
class play_list;
}

class play_list : public QFrame
{
        Q_OBJECT

public:
        explicit play_list(QWidget *parent = 0);
        ~play_list();

private:
        Ui::play_list *ui;
};

#endif // PLAY_LIST_H
