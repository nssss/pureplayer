/********************************************************************************
** Form generated from reading UI file 'play_list.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PLAY_LIST_H
#define UI_PLAY_LIST_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>

QT_BEGIN_NAMESPACE

class Ui_play_list
{
public:
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QLineEdit *search_edit;
    QPushButton *search_btn;
    QListView *playlist_view;

    void setupUi(QFrame *play_list)
    {
        if (play_list->objectName().isEmpty())
            play_list->setObjectName(QStringLiteral("play_list"));
        play_list->resize(263, 441);
        play_list->setFrameShape(QFrame::StyledPanel);
        play_list->setFrameShadow(QFrame::Raised);
        verticalLayout = new QVBoxLayout(play_list);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        search_edit = new QLineEdit(play_list);
        search_edit->setObjectName(QStringLiteral("search_edit"));

        horizontalLayout->addWidget(search_edit);

        search_btn = new QPushButton(play_list);
        search_btn->setObjectName(QStringLiteral("search_btn"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/resource/resource/icon/32px/MAGNIFYING_GLASS_32px.png"), QSize(), QIcon::Normal, QIcon::On);
        search_btn->setIcon(icon);
        search_btn->setFlat(true);

        horizontalLayout->addWidget(search_btn);


        verticalLayout->addLayout(horizontalLayout);

        playlist_view = new QListView(play_list);
        playlist_view->setObjectName(QStringLiteral("playlist_view"));
        playlist_view->setFrameShape(QFrame::StyledPanel);
        playlist_view->setFrameShadow(QFrame::Sunken);

        verticalLayout->addWidget(playlist_view);


        retranslateUi(play_list);

        QMetaObject::connectSlotsByName(play_list);
    } // setupUi

    void retranslateUi(QFrame *play_list)
    {
        play_list->setWindowTitle(QApplication::translate("play_list", "Frame", Q_NULLPTR));
        search_btn->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class play_list: public Ui_play_list {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PLAY_LIST_H
