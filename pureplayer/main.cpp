﻿
#include "pure_player.h"
#include <QApplication>
#undef main

int main(int argc, char *argv[])
{
        QApplication app(argc, argv);

        skin_helper::set_style(":/resource/qss/style.qss");

        pure_player player;
        player.show();

        return app.exec();
}
