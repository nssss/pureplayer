﻿#ifndef CONTROL_BAR_H
#define CONTROL_BAR_H

#include <QString>
#include <QFrame>

namespace Ui {
class control_bar;
}

class control_bar : public QFrame
{
        Q_OBJECT

public:
        explicit control_bar(QWidget *parent = 0);
        ~control_bar();

signals:
        void open_playlist();
        void modify_media_volume(int val);
        void modify_media_progress(int val);
        void operate_media(bool pause);
        void change_display_media(const QString& val);

private slots:
        void on_progress_slider_actionTriggered(int action);
        void on_volume_slider_actionTriggered(int action);
        void on_playlist_btn_clicked();
        void on_prev_btn_clicked();
        void on_play_pause_btn_clicked();
        void on_next_btn_clicked();

public:
        void set_volume_slider_range(int val);
        void set_volume_slider_value(int val);
        void set_progress_slider_range(int val);
        void set_progress_slider_value(int val);

private:
        QString to_time_string(int val);

private:
        bool _b_paused;
        Ui::control_bar *ui;
        const QString _str_icon_base_url;
};

#endif // CONTROL_BAR_H
