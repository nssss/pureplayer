/********************************************************************************
** Form generated from reading UI file 'pure_player.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PURE_PLAYER_H
#define UI_PURE_PLAYER_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_pure_player
{
public:

    void setupUi(QWidget *pure_player)
    {
        if (pure_player->objectName().isEmpty())
            pure_player->setObjectName(QStringLiteral("pure_player"));
        pure_player->resize(908, 593);
        pure_player->setAcceptDrops(true);

        retranslateUi(pure_player);

        QMetaObject::connectSlotsByName(pure_player);
    } // setupUi

    void retranslateUi(QWidget *pure_player)
    {
        pure_player->setWindowTitle(QApplication::translate("pure_player", "Widget", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class pure_player: public Ui_pure_player {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PURE_PLAYER_H
