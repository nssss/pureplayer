﻿#include "title_bar.h"
#include "ui_title_bar.h"

title_bar::title_bar(QWidget *parent) :
        QFrame(parent),
        ui(new Ui::title_bar),
        _str_icon_base_url(":/resource/icon/32px/")
{
        ui->setupUi(this);

        setMouseTracking(true);
        _b_maximized_wnd = false;
        setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint|Qt::Tool);

        QString icon_url = _str_icon_base_url + "MAXIMIZE_32px.png";
        ui->max_btn->setIcon(QIcon(icon_url));
}

title_bar::~title_bar()
{
        delete ui;
}

void title_bar::on_close_btn_clicked()
{
        emit close_window();
}

void title_bar::on_max_btn_clicked()
{
        emit maximize_window(_b_maximized_wnd);

        _b_maximized_wnd = !_b_maximized_wnd;
        QString icon_url = _str_icon_base_url + (_b_maximized_wnd ? "COLLAPSE_32px.png" : "MAXIMIZE_32px.png");
        ui->max_btn->setIcon(QIcon(icon_url));
}

void title_bar::on_min_btn_clicked()
{
        emit minimize_window();
}

void title_bar::on_config_btn_clicked()
{
        emit open_config_panel();
}

void title_bar::set_title(const QString& title)
{
        ui->title_label->setText(title);
}
