/********************************************************************************
** Form generated from reading UI file 'control_bar.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONTROL_BAR_H
#define UI_CONTROL_BAR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>

QT_BEGIN_NAMESPACE

class Ui_control_bar
{
public:
    QHBoxLayout *horizontalLayout_4;
    QHBoxLayout *horizontalLayout;
    QLabel *cur_time_label;
    QLabel *duration_lable;
    QSlider *progress_slider;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *prev_btn;
    QPushButton *play_pause_btn;
    QPushButton *next_btn;
    QHBoxLayout *horizontalLayout_3;
    QSlider *volume_slider;
    QPushButton *playlist_btn;

    void setupUi(QFrame *control_bar)
    {
        if (control_bar->objectName().isEmpty())
            control_bar->setObjectName(QStringLiteral("control_bar"));
        control_bar->resize(882, 46);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(control_bar->sizePolicy().hasHeightForWidth());
        control_bar->setSizePolicy(sizePolicy);
        control_bar->setFrameShape(QFrame::StyledPanel);
        control_bar->setFrameShadow(QFrame::Raised);
        horizontalLayout_4 = new QHBoxLayout(control_bar);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        cur_time_label = new QLabel(control_bar);
        cur_time_label->setObjectName(QStringLiteral("cur_time_label"));
        cur_time_label->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout->addWidget(cur_time_label);

        duration_lable = new QLabel(control_bar);
        duration_lable->setObjectName(QStringLiteral("duration_lable"));
        duration_lable->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignVCenter);

        horizontalLayout->addWidget(duration_lable);


        horizontalLayout_4->addLayout(horizontalLayout);

        progress_slider = new QSlider(control_bar);
        progress_slider->setObjectName(QStringLiteral("progress_slider"));
        progress_slider->setOrientation(Qt::Horizontal);

        horizontalLayout_4->addWidget(progress_slider);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        prev_btn = new QPushButton(control_bar);
        prev_btn->setObjectName(QStringLiteral("prev_btn"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/resource/resource/icon/32px/BACKWARD_32px.png"), QSize(), QIcon::Normal, QIcon::On);
        prev_btn->setIcon(icon);
        prev_btn->setAutoDefault(false);
        prev_btn->setFlat(false);

        horizontalLayout_2->addWidget(prev_btn);

        play_pause_btn = new QPushButton(control_bar);
        play_pause_btn->setObjectName(QStringLiteral("play_pause_btn"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/resource/resource/icon/32px/PLAY_32px.png"), QSize(), QIcon::Normal, QIcon::On);
        play_pause_btn->setIcon(icon1);
        play_pause_btn->setAutoDefault(false);
        play_pause_btn->setFlat(false);

        horizontalLayout_2->addWidget(play_pause_btn);

        next_btn = new QPushButton(control_bar);
        next_btn->setObjectName(QStringLiteral("next_btn"));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/resource/resource/icon/32px/FORWARD_32px.png"), QSize(), QIcon::Normal, QIcon::On);
        next_btn->setIcon(icon2);
        next_btn->setAutoDefault(false);
        next_btn->setFlat(false);

        horizontalLayout_2->addWidget(next_btn);


        horizontalLayout_4->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        volume_slider = new QSlider(control_bar);
        volume_slider->setObjectName(QStringLiteral("volume_slider"));
        volume_slider->setOrientation(Qt::Horizontal);

        horizontalLayout_3->addWidget(volume_slider);

        playlist_btn = new QPushButton(control_bar);
        playlist_btn->setObjectName(QStringLiteral("playlist_btn"));
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/resource/resource/icon/32px/HAMBURGER_MENU_32px.png"), QSize(), QIcon::Normal, QIcon::On);
        playlist_btn->setIcon(icon3);
        playlist_btn->setAutoDefault(false);
        playlist_btn->setFlat(false);

        horizontalLayout_3->addWidget(playlist_btn);


        horizontalLayout_4->addLayout(horizontalLayout_3);

        horizontalLayout_4->setStretch(1, 8);

        retranslateUi(control_bar);

        QMetaObject::connectSlotsByName(control_bar);
    } // setupUi

    void retranslateUi(QFrame *control_bar)
    {
        control_bar->setWindowTitle(QApplication::translate("control_bar", "Frame", Q_NULLPTR));
        cur_time_label->setText(QApplication::translate("control_bar", "ct", Q_NULLPTR));
        duration_lable->setText(QApplication::translate("control_bar", "dt", Q_NULLPTR));
        prev_btn->setText(QString());
        play_pause_btn->setText(QString());
        next_btn->setText(QString());
        playlist_btn->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class control_bar: public Ui_control_bar {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONTROL_BAR_H
