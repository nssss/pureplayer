#ifndef TITLE_BAR_H
#define TITLE_BAR_H

#include <QFrame>
#include "type_def.h"

namespace Ui {
class title_bar;
}

class title_bar : public QFrame
{
        Q_OBJECT

public:
        explicit title_bar(QWidget *parent = 0);
        ~title_bar();

signals:
        void close_window();
        void minimize_window();
        void maximize_window(bool restore);
        void open_config_panel();

private slots:
        void on_close_btn_clicked();
        void on_max_btn_clicked();
        void on_min_btn_clicked();
        void on_config_btn_clicked();

public:
        void set_title(const QString& title);

private:
        Ui::title_bar *ui;

private:
        bool _b_maximized_wnd;
        const QString _str_icon_base_url;
};

#endif // TITLE_BAR_H
