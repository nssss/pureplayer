/********************************************************************************
** Form generated from reading UI file 'config_panel.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CONFIG_PANEL_H
#define UI_CONFIG_PANEL_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>

QT_BEGIN_NAMESPACE

class Ui_config_panel
{
public:

    void setupUi(QDialog *config_panel)
    {
        if (config_panel->objectName().isEmpty())
            config_panel->setObjectName(QStringLiteral("config_panel"));
        config_panel->resize(541, 387);

        retranslateUi(config_panel);

        QMetaObject::connectSlotsByName(config_panel);
    } // setupUi

    void retranslateUi(QDialog *config_panel)
    {
        config_panel->setWindowTitle(QApplication::translate("config_panel", "Dialog", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class config_panel: public Ui_config_panel {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CONFIG_PANEL_H
