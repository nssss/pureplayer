﻿#include "control_bar.h"
#include "ui_control_bar.h"

#define SPACE_FILL(x) (x < 10 ? 0##x : #x)

control_bar::control_bar(QWidget *parent) :
        QFrame(parent),
        ui(new Ui::control_bar),
      _str_icon_base_url(":/resource/icon/32px/")
{
        ui->setupUi(this);

        _b_paused = false;
        setMouseTracking(true);
        setWindowFlags(Qt::FramelessWindowHint|Qt::WindowStaysOnTopHint|Qt::Tool);

        QString init_zero = to_time_string(0);
        ui->cur_time_label->setText(init_zero);
        ui->duration_lable->setText("|" + init_zero);

        QString icon_url = _str_icon_base_url + "PAUSE_32px.png";
        ui->play_pause_btn->setIcon(QIcon(icon_url));
}

control_bar::~control_bar()
{
        delete ui;
}

void control_bar::on_progress_slider_actionTriggered(int action)
{
        emit modify_media_progress(action);
}

void control_bar::on_volume_slider_actionTriggered(int action)
{
        emit modify_media_volume(action);
}

void control_bar::on_playlist_btn_clicked()
{

}

void control_bar::on_prev_btn_clicked()
{

}

void control_bar::on_play_pause_btn_clicked()
{
        _b_paused = !_b_paused;
        emit operate_media(_b_paused);

        QString icon_url = _str_icon_base_url + (_b_paused ? "PLAY_32px.png" : "PAUSE_32px.png");
        ui->play_pause_btn->setIcon(QIcon(icon_url));
}

void control_bar::on_next_btn_clicked()
{

}

void control_bar::set_volume_slider_range(int val)
{
        ui->volume_slider->setRange(0, val);
        ui->volume_slider->setMouseTracking(true);
}

void control_bar::set_volume_slider_value(int val)
{
        ui->volume_slider->setValue(val);
}

void control_bar::set_progress_slider_range(int val)
{
        _b_paused = false;

        ui->progress_slider->setRange(0, val);
        ui->progress_slider->setMouseTracking(true);

        QString duration = "|" + to_time_string(val);
        ui->duration_lable->setText(duration);
}

void control_bar::set_progress_slider_value(int val)
{
        QString cur_time = to_time_string(val);
        ui->cur_time_label->setText(cur_time);
        ui->progress_slider->setValue(val);
}

QString control_bar::to_time_string(int val)
{
        val /= 1000;
        int h = val / 3600;
        int s = val % 60;
        int m = (val - h * 3600 - s) / 60;

        QString format;
        format = h < 10 ? "0%1:" : "%1:";
        format += m < 10 ? "0%2:" : "%2:";
        format += s < 10 ? "0%3" : "%3";

        return QString(format).arg(h).arg(m).arg(s);
}
