/********************************************************************************
** Form generated from reading UI file 'title_bar.ui'
**
** Created by: Qt User Interface Compiler version 5.9.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TITLE_BAR_H
#define UI_TITLE_BAR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>

QT_BEGIN_NAMESPACE

class Ui_title_bar
{
public:
    QHBoxLayout *horizontalLayout_2;
    QPushButton *config_btn;
    QSpacerItem *horizontalSpacer;
    QLabel *title_label;
    QSpacerItem *horizontalSpacer_2;
    QHBoxLayout *horizontalLayout;
    QPushButton *min_btn;
    QPushButton *max_btn;
    QPushButton *close_btn;

    void setupUi(QFrame *title_bar)
    {
        if (title_bar->objectName().isEmpty())
            title_bar->setObjectName(QStringLiteral("title_bar"));
        title_bar->setWindowModality(Qt::NonModal);
        title_bar->resize(762, 46);
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(title_bar->sizePolicy().hasHeightForWidth());
        title_bar->setSizePolicy(sizePolicy);
        title_bar->setFrameShape(QFrame::StyledPanel);
        title_bar->setFrameShadow(QFrame::Raised);
        horizontalLayout_2 = new QHBoxLayout(title_bar);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        config_btn = new QPushButton(title_bar);
        config_btn->setObjectName(QStringLiteral("config_btn"));
        config_btn->setMinimumSize(QSize(16, 16));
        config_btn->setFlat(false);

        horizontalLayout_2->addWidget(config_btn);

        horizontalSpacer = new QSpacerItem(31, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer);

        title_label = new QLabel(title_bar);
        title_label->setObjectName(QStringLiteral("title_label"));
        title_label->setAlignment(Qt::AlignCenter);

        horizontalLayout_2->addWidget(title_label);

        horizontalSpacer_2 = new QSpacerItem(31, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        min_btn = new QPushButton(title_bar);
        min_btn->setObjectName(QStringLiteral("min_btn"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(min_btn->sizePolicy().hasHeightForWidth());
        min_btn->setSizePolicy(sizePolicy1);
        min_btn->setMinimumSize(QSize(16, 16));
        min_btn->setAutoDefault(false);
        min_btn->setFlat(false);

        horizontalLayout->addWidget(min_btn);

        max_btn = new QPushButton(title_bar);
        max_btn->setObjectName(QStringLiteral("max_btn"));
        max_btn->setMinimumSize(QSize(16, 16));
        max_btn->setAutoDefault(false);
        max_btn->setFlat(false);

        horizontalLayout->addWidget(max_btn);

        close_btn = new QPushButton(title_bar);
        close_btn->setObjectName(QStringLiteral("close_btn"));
        close_btn->setMinimumSize(QSize(16, 16));
        close_btn->setAutoDefault(false);
        close_btn->setFlat(false);

        horizontalLayout->addWidget(close_btn);


        horizontalLayout_2->addLayout(horizontalLayout);

        horizontalLayout_2->setStretch(1, 1);
        horizontalLayout_2->setStretch(2, 8);
        horizontalLayout_2->setStretch(3, 1);

        retranslateUi(title_bar);

        min_btn->setDefault(false);
        max_btn->setDefault(false);
        close_btn->setDefault(false);


        QMetaObject::connectSlotsByName(title_bar);
    } // setupUi

    void retranslateUi(QFrame *title_bar)
    {
        title_bar->setWindowTitle(QApplication::translate("title_bar", "Frame", Q_NULLPTR));
        config_btn->setText(QString());
        title_label->setText(QApplication::translate("title_bar", "title", Q_NULLPTR));
        min_btn->setText(QString());
        max_btn->setText(QString());
        close_btn->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class title_bar: public Ui_title_bar {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TITLE_BAR_H
