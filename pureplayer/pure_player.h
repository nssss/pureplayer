﻿#ifndef PURE_PLAYER_H
#define PURE_PLAYER_H

#include <QSize>
#include <QMap>
#include <QWidget>
#include <QMenu>
#include <QTimer>
#include <QFile>
#include <QApplication>
#include <QContextMenuEvent>
#include <QGraphicsOpacityEffect>
#include "type_def.h"
#include "title_bar.h"
#include "control_bar.h"
#include "SDLDisplayerManager.h"

namespace Ui {
class pure_player;
}

class skin_helper
{
public:
    static bool set_style(const QString& style)
    {
        QFile qss(style);
        if(!qss.open(QFile::ReadOnly)){
                return false;
        }

        qApp->setStyleSheet(qss.readAll());
        qss.close();
        return true;
    }
};

class pure_player : public QWidget
{
        Q_OBJECT

public:
        explicit pure_player(QWidget *parent = 0);
        ~pure_player();

public slots:
        //title bar
        void close_window();
        void minimize_window();
        void maximize_window(bool restore);
        void open_config_panel();

        //control bar
        void open_playlist();
        void update_display_time();
        void modify_media_volume(int val);
        void modify_media_progress(int val);
        void operate_media(bool pause);
        void change_display_media(const QString& val);

        //menu
        void open_media_file();

private:
        int play_media_file(const char* media_file);

private:
        void init_ui();
        void create_popup_menu();

private:
        void set_cursor(const QPoint& cursor_pt);
        void move_window(QMouseEvent *event);
        void load_title_control_bar(const QRect& bar_rt, const QPoint& cursor_pt, int bar_type);
        void show_with_animation(QGraphicsOpacityEffect* effect, QFrame* frm, const QRect& rt, double sv, double ev, int duration);

private:
        void wheelEvent(QWheelEvent *event);
        void mouseMoveEvent(QMouseEvent *event);
        void mousePressEvent(QMouseEvent * event);
        void mouseReleaseEvent(QMouseEvent *event);
        void mouseDoubleClickEvent(QMouseEvent *event);

        void contextMenuEvent(QContextMenuEvent *event);

        void dragEnterEvent(QDragEnterEvent *e);
        void dropEvent(QDropEvent *e);

private:
        Ui::pure_player *ui;

        QPoint _drag_point;
        bool _b_left_pressed;
        EDirectionType _dir_type;

        QRect _old_rect;
        bool _b_fullscreen;

private:
        enum{eBar_title = 0, eBar_control, eBar_num};
        int _bar_height[eBar_num];
        QFrame* _bar_frame[eBar_num];
        QGraphicsOpacityEffect* _bar_opacity_effect[eBar_num];

private:
        QString _cur_file_path;
        QTimer* _disp_time_timer;
        smg_player::SDLDisplayerManager* _sdl_mgr;

private:
        QMenu* _pop_menu;
        typedef QMap<EMenuActionType, QAction*> ActionMap;
        ActionMap _action_map;
};

#endif // PURE_PLAYER_H
