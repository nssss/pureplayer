#-------------------------------------------------
#
# Project created by QtCreator 2018-02-08T16:34:26
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = pureplayer
TEMPLATE = app

CONFIG(debug, debug|release){
    DESTDIR = $$PWD/../bin/debug/
}else{
    DESTDIR = $$PWD/../bin/release/
}

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        pure_player.cpp \
    title_bar.cpp \
    control_bar.cpp \
    config_panel.cpp \
    play_list.cpp

HEADERS += \
        pure_player.h \
    title_bar.h \
    control_bar.h \
    config_panel.h \
    play_list.h \
    type_def.h

FORMS += \
        pure_player.ui \
    title_bar.ui \
    control_bar.ui \
    config_panel.ui \
    play_list.ui

INCLUDEPATH +=  ../3rdParty/include
INCLUDEPATH +=  ../3rdParty/include/sdl2.0.5

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../3rdParty/lib/SDL -lSDL2
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../3rdParty/lib/SDL -lSDL2
else:unix:!macx: LIBS += -L$$PWD/../3rdParty/lib/SDL -lSDL2

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../3rdParty/lib/SDL -lSDL2main
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../3rdParty/lib/SDL -lSDL2main
else:unix:!macx: LIBS += -L$$PWD/../3rdParty/lib/SDL -lSDL2main

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../bin/release/ -lcommon
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../bin/debug/ -lcommon
else:unix:!macx: LIBS += -L$$PWD/../common/ -lcommon

INCLUDEPATH += $$PWD/../common
DEPENDPATH += $$PWD/../common

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../bin/release/ -lsdldisp
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../bin/debug/ -lsdldisp
else:unix: LIBS += -L$$PWD/../sdldisp/ -lsdldisp

INCLUDEPATH += $$PWD/../sdldisp
DEPENDPATH += $$PWD/../sdldisp

RESOURCES += \
    resource.qrc
