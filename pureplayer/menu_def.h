#ifndef MENUDEF_H
#define MENUDEF_H

#include <QString>

typedef enum EMenuCategory{
        eMenuCategory_1st  = 0x1000,
        eMenuCategory_2nd = 0x2000,
        eMenuCategory_3rd  = 0x3000,
}EMenuCategory;

#define MENU_1ST(x)   (eMenuCategory_1st  | x)
#define MENU_2ND(x)  (eMenuCategory_2nd | x)
#define MENU_3RD(x)   (eMenuCategory_3rd | x)

typedef enum EMenuActionType{
        eAction_undef = -1,

        //1st
        eAction_open   = MENU_1ST(1),
        eAction_close   = MENU_1ST(2),
        eAction_synch   = MENU_1ST(3),
        eAction_video   = MENU_1ST(4),
        eAction_audio   = MENU_1ST(5),
        eAction_subtitle = MENU_1ST(6),
        eAction_clip      = MENU_1ST(7),
        eAction_skin     = MENU_1ST(8),
        eAction_option  = MENU_1ST(9),

        //2nd
        eAction_video_image = MENU_2ND(1),
        eAction_synch_forward = MENU_2ND(2),
        eAction_synch_backward = MENU_2ND(3),

        //3rd
        eAction_video_image_origin   = MENU_3RD(1),
        eAction_video_image_16_9   = MENU_3RD(2),
        eAction_video_image_4_3     = MENU_3RD(3),
        eAction_video_image_x_0d5 = MENU_3RD(4),
        eAction_video_image_x_1d5 = MENU_3RD(5),
}EMenuActionType;


#endif // MENUDEF_H
