﻿
#include "pure_player.h"
#include "ui_pure_player.h"
#include "SMGErrorType.h"

#include <QFileDialog>
#include <QMimeData>
#include <QMessageBox>
#include <QPropertyAnimation>

#define PADDING 5

QString Menu_1st_Text_Arr[] ={
        "打开",
        "关闭",
        "同步",
        "视频",
        "音频",
        "字幕",
        "截取",
        "皮肤",
        "选项",
};

QString Menu_2nd_Text_Arr[] = {
        "画面",
        "向前",
        "向后",
};

QString Menu_3rd_Text_Arr[] = {
        "原画",
        "16:9",
        "4:3",
        "0.5x",
        "1.5x",
};

pure_player::pure_player(QWidget *parent) :
        QWidget(parent),
        ui(new Ui::pure_player)
{
        ui->setupUi(this);
        init_ui();
}

pure_player::~pure_player()
{
        if(_disp_time_timer->isActive()){
                _disp_time_timer->stop();
                delete _disp_time_timer;
                _disp_time_timer = nullptr;
        }

        for(size_t i=0; i<eBar_num; ++i){
                if(_bar_opacity_effect[i]){
                        delete _bar_opacity_effect[i];
                        _bar_opacity_effect[i] = nullptr;
                }

                if(_bar_frame[i]){
                        delete _bar_frame[i];
                        _bar_frame[i] = nullptr;
                }
        }

        delete ui;
}

void pure_player::init_ui()
{
        setWindowTitle("pure");
        setWindowIcon(QIcon(":/resource/icon/32px/DROP_32px.png"));

         //去掉标题栏
        setWindowFlags(Qt::FramelessWindowHint | Qt::WindowMinimizeButtonHint);
        setMouseTracking(true);

        //创建子菜单项
        create_popup_menu();

        //创建title / control bar
        _bar_frame[eBar_title] = new title_bar(nullptr);
        _bar_frame[eBar_control] = new control_bar(nullptr);
        for(size_t i=0; i<eBar_num; ++i){
                _bar_height[i] = _bar_frame[i]->geometry().height();
                _bar_opacity_effect[i] = new QGraphicsOpacityEffect(_bar_frame[i]);
                _bar_opacity_effect[i]->setOpacity(0.0);
                _bar_frame[i]->setGraphicsEffect(_bar_opacity_effect[i]);
        }

        connect((title_bar*)_bar_frame[eBar_title], &title_bar::close_window, this, &pure_player::close_window);
        connect((title_bar*)_bar_frame[eBar_title], &title_bar::minimize_window, this, &pure_player::minimize_window);
        connect((title_bar*)_bar_frame[eBar_title], &title_bar::maximize_window, this, &pure_player::maximize_window);
        connect((title_bar*)_bar_frame[eBar_title], &title_bar::open_config_panel, this, &pure_player::open_config_panel);

        connect((control_bar*)_bar_frame[eBar_control], &control_bar::open_playlist, this, &pure_player::open_playlist);
        connect((control_bar*)_bar_frame[eBar_control], &control_bar::modify_media_volume, this, &pure_player::modify_media_volume);
        connect((control_bar*)_bar_frame[eBar_control], &control_bar::modify_media_progress, this, &pure_player::modify_media_progress);
        connect((control_bar*)_bar_frame[eBar_control], &control_bar::operate_media, this, &pure_player::operate_media);
        connect((control_bar*)_bar_frame[eBar_control], &control_bar::change_display_media, this, &pure_player::change_display_media);

        _b_fullscreen = false;
        _b_left_pressed = false;
        _sdl_mgr = smg_player::SDLDisplayerManager::instance();
        ((control_bar*)_bar_frame[eBar_control])->set_volume_slider_range(120);

        _disp_time_timer = new QTimer(this);
        _disp_time_timer->setInterval(1000);
        connect(_disp_time_timer, &QTimer::timeout, this, &pure_player::update_display_time);
}

void pure_player::create_popup_menu()
{
        //创建菜单、菜单项
        _pop_menu = new QMenu();

        for(int i=eAction_open; i<=eAction_option; ++i){
                QAction* action = new QAction(Menu_1st_Text_Arr[(i & 0x0fff) - 1], this);
                _action_map[(EMenuActionType)i] = action;
        }

        //video
        QAction* action_video_image = new QAction(this);
        action_video_image->setText(Menu_2nd_Text_Arr[0]);
        _action_map[eAction_video_image] = action_video_image;

        for(int i=eAction_video_image_origin; i<=eAction_video_image_x_1d5; ++i){
                QAction* action = new QAction(Menu_3rd_Text_Arr[(i & 0x0fff) - 1], this);
                _action_map[(EMenuActionType)i] = action;
        }

        //synch
        for(int i=eAction_synch_forward; i<=eAction_synch_backward; ++i){
                QAction* action = new QAction(Menu_2nd_Text_Arr[(i & 0x0fff) - 1], this);
                _action_map[(EMenuActionType)i] = action;
        }

        connect(_action_map[eAction_open], &QAction::triggered, this, &pure_player::open_media_file);
}

void pure_player::contextMenuEvent(QContextMenuEvent *event)
{
        //清除原有菜单
        _pop_menu->clear();

        //open / close
        _pop_menu->addAction(_action_map[eAction_open]);
        _pop_menu->addAction(_action_map[eAction_close]);

        //synch
        QMenu* synch_menu = _pop_menu->addMenu(Menu_1st_Text_Arr[(eAction_synch & 0x0fff) - 1]);
        synch_menu->addAction(_action_map[eAction_synch_forward]);
        synch_menu->addAction(_action_map[eAction_synch_backward]);

        //video
        QMenu* video_menu = _pop_menu->addMenu(Menu_1st_Text_Arr[(eAction_video & 0x0fff) - 1]);
        QMenu* video_image_menu = video_menu->addMenu(Menu_2nd_Text_Arr[0]);
        for(int i=eAction_video_image_origin; i<=eAction_video_image_x_1d5; ++i){
                video_image_menu->addAction(_action_map[(EMenuActionType)i]);
        }

        _pop_menu->addAction(_action_map[eAction_audio]);
        _pop_menu->addAction(_action_map[eAction_subtitle]);
        _pop_menu->addAction(_action_map[eAction_clip]);
        _pop_menu->addAction(_action_map[eAction_skin]);
        _pop_menu->addAction(_action_map[eAction_option]);

        _pop_menu->exec(QCursor::pos());
        event->accept();
}

void pure_player::dragEnterEvent(QDragEnterEvent *e)
{
        if(e->mimeData()->hasFormat("text/uri-list")){
                e->acceptProposedAction();
        }
}

void pure_player::dropEvent(QDropEvent *e)
{
        QList<QUrl> urls = e->mimeData()->urls();
        if(urls.isEmpty()){
                return;
        }

        _cur_file_path = urls.first().toLocalFile();
        if(_cur_file_path.isEmpty()){
                return;
        }

        int ret = eSHErr_success;
        if((ret = play_media_file(_cur_file_path.toStdString().c_str())) != eSHErr_success){
               QString tips = QString("open file [{0}] failed.").arg(_cur_file_path);
               QMessageBox::information(this, "tips", tips, QMessageBox::Ok, QMessageBox::Ok);
                return;
        }

        QString title = _cur_file_path.mid(_cur_file_path.lastIndexOf('/') + 1);
        ((title_bar*)_bar_frame[eBar_title])->set_title(title);
}

void pure_player::wheelEvent(QWheelEvent *event)
{

}

void pure_player::mousePressEvent(QMouseEvent * event)
{
        Qt::MouseButtons mbtn = event->buttons();
        if(mbtn & Qt::LeftButton){
                _b_left_pressed = true;
                if(_dir_type == eDirection_undef) {
                    _drag_point = event->globalPos() - frameGeometry().topLeft();
                }
        }

        QWidget::mousePressEvent(event);
}

void pure_player::mouseMoveEvent(QMouseEvent *event)
{
        QRect bar_rt[eBar_num];
        const QRect& player_rt = this->geometry();

        QSize title_size(player_rt.width(), _bar_height[eBar_title]);
        QPoint title_top_left_pt = mapToGlobal(QPoint(0, 0));
        bar_rt[eBar_title] = QRect(title_top_left_pt, title_size);

        QSize control_size(player_rt.width(), _bar_height[eBar_control]);
        QPoint control_top_left_pt = title_top_left_pt + QPoint(0, player_rt.height() - _bar_height[eBar_control]);
        bar_rt[eBar_control] = QRect(control_top_left_pt, control_size);

        for(size_t i=0; i<eBar_num; ++i){
                _bar_frame[i]->resize(bar_rt[i].size());
                _bar_frame[i]->move(bar_rt[i].topLeft());
                load_title_control_bar(bar_rt[i], event->globalPos(), i);
        }

        move_window(event);
}

 void pure_player::mouseReleaseEvent(QMouseEvent *event)
 {
        _b_left_pressed = false;
        if(_dir_type != eDirection_undef) {
                setCursor(QCursor(Qt::ArrowCursor));
        }

        int ret = eSHErr_success;
        if(!_cur_file_path.isEmpty()){
                ret = _sdl_mgr->update_displayer(_cur_file_path.toStdString().c_str());
        }
 }

 void pure_player::mouseDoubleClickEvent(QMouseEvent *event)
 {
         Qt::MouseButtons mbtn = event->buttons();
         if((mbtn & Qt::LeftButton) == 0){
                 return;
         }

         _b_fullscreen = !_b_fullscreen;
         if(_b_fullscreen){
                 _old_rect = geometry();
                 setWindowState(Qt::WindowFullScreen);
         }
         else{
                 setGeometry(_old_rect);
         }

         int ret = eSHErr_success;
         if(!_cur_file_path.isEmpty()){
                 ret = _sdl_mgr->update_displayer(_cur_file_path.toStdString().c_str());
         }
 }

 void pure_player::show_with_animation(QGraphicsOpacityEffect* effect, QFrame* frm, const QRect& rt, double sv, double ev, int duration)
 {
//         frm->resize(rt.size());
//         frm->move(rt.topLeft());

         QPropertyAnimation* anim_ptr = new QPropertyAnimation(effect, "opacity", this);
         anim_ptr->setDuration(duration);
         anim_ptr->setStartValue(sv);
         anim_ptr->setEndValue(ev);
         anim_ptr->start(QAbstractAnimation::DeleteWhenStopped);
 }

 void pure_player::load_title_control_bar(const QRect& bar_rt, const QPoint& cursor_pt, int bar_type)
 {
         static bool last_bar_show[eBar_num] = {false, false};
         if(!last_bar_show[bar_type] && bar_rt.contains(cursor_pt)){
                 last_bar_show[bar_type] = true;
                 _bar_frame[bar_type]->show();
                 show_with_animation(_bar_opacity_effect[bar_type], _bar_frame[bar_type], bar_rt, 0.0, 1.0, 250);
         }
         else if(last_bar_show[bar_type] && !bar_rt.contains(cursor_pt)){
                 last_bar_show[bar_type] = false;
                 show_with_animation(_bar_opacity_effect[bar_type],  _bar_frame[bar_type], bar_rt, 1.0, 0.0, 750);
                 _bar_frame[bar_type]->hide();
         }
 }

 void pure_player::move_window(QMouseEvent *event)
 {
         QRect rect = this->rect();
         QPoint tl = mapToGlobal(rect.topLeft());
         QPoint rb = mapToGlobal(rect.bottomRight());
         QPoint global_pos = event->globalPos();

         if(!_b_left_pressed){
                 set_cursor(global_pos);
                 return;
         }

         if(_dir_type == eDirection_undef){
                 move(global_pos - _drag_point);
                 event->accept();
                 return;
         }

         QRect new_rt(tl, rb);

         switch(_dir_type) {
         case eDirection_left:{
                 if(rb.x() - global_pos.x() <= minimumWidth()) {
                         new_rt.setX(tl.x());
                 }
                 else{
                         new_rt.setX(global_pos.x());
                 }
                 break;
         }
         case eDirection_right:{
                 new_rt.setWidth(global_pos.x() - tl.x());
                 break;
         }
         case eDirection_up:{
                 if(rb.y() - global_pos.y() <= minimumHeight()){
                         new_rt.setY(tl.y());
                 }
                 else{
                         new_rt.setY(global_pos.y());
                 }
                 break;
         }
         case eDirection_down:{
                 new_rt.setHeight(global_pos.y() - tl.y());
                 break;
         }
         case eDirection_left_top:{
                 if(rb.x() - global_pos.x() <= minimumWidth()){
                         new_rt.setX(tl.x());
                 }
                 else{
                         new_rt.setX(global_pos.x());
                 }
                 if(rb.y() - global_pos.y() <= minimumHeight()){
                         new_rt.setY(tl.y());
                 }
                 else{
                         new_rt.setY(global_pos.y());
                 }
                 break;
         }
         case eDirection_right_top:{
                 new_rt.setWidth(global_pos.x() - tl.x());
                 new_rt.setY(global_pos.y());
                 break;
         }
         case eDirection_left_bottom:{
                 new_rt.setX(global_pos.x());
                 new_rt.setHeight(global_pos.y() - tl.y());
                 break;
         }
         case eDirection_right_bottom:{
                 new_rt.setWidth(global_pos.x() - tl.x());
                 new_rt.setHeight(global_pos.y() - tl.y());
                 break;
         }
         default:
                 break;
         }

         setGeometry(new_rt);

         QWidget::mouseMoveEvent(event);
 }

void pure_player::set_cursor(const QPoint& cursor_pt)
{
        int x = cursor_pt.x();
        int y = cursor_pt.y();
        Qt::CursorShape cursor_shape;

        QRect rect = this->rect();
        QPoint tl = mapToGlobal(rect.topLeft());
        QPoint rb = mapToGlobal(rect.bottomRight());

        if(tl.x() + PADDING >= x && tl.x() <= x && tl.y() + PADDING >= y && tl.y() <= y) {                  // 左上
                _dir_type = eDirection_left_top; cursor_shape = Qt::SizeFDiagCursor;
        } else if(x >= rb.x() - PADDING && x <= rb.x() && y >= rb.y() - PADDING && y <= rb.y()) {     // 右下
               _dir_type = eDirection_right_bottom; cursor_shape = Qt::SizeFDiagCursor;
        } else if(x <= tl.x() + PADDING && x >= tl.x() && y >= rb.y() - PADDING && y <= rb.y()) {       //左下
                _dir_type = eDirection_left_bottom; cursor_shape = Qt::SizeBDiagCursor;
        } else if(x <= rb.x() && x >= rb.x() - PADDING && y >= tl.y() && y <= tl.y() + PADDING) {      // 右上
                _dir_type = eDirection_right_top; cursor_shape = Qt::SizeBDiagCursor;
        } else if(x <= tl.x() + PADDING && x >= tl.x()) {                                       // 左
                _dir_type = eDirection_left; cursor_shape = Qt::SizeHorCursor;
        } else if( x <= rb.x() && x >= rb.x() - PADDING) {                                     // 右
                _dir_type = eDirection_right; cursor_shape = Qt::SizeHorCursor;
        }else if(y >= tl.y() && y <= tl.y() + PADDING){                                        // 上
                _dir_type = eDirection_up; cursor_shape = Qt::SizeVerCursor;
        } else if(y <= rb.y() && y >= rb.y() - PADDING) {                                     // 下
                _dir_type = eDirection_down; cursor_shape = Qt::SizeVerCursor;
        }else {                                                                                                       //default
                _dir_type = eDirection_undef; cursor_shape = Qt::ArrowCursor;
        }

        setCursor(QCursor(cursor_shape));
}

//title_bar
void pure_player::close_window()
{
        close();
}

void pure_player::minimize_window()
{
        setWindowState(Qt::WindowMinimized);
}

void pure_player::maximize_window(bool restore)
{
        if(!restore){
                _old_rect = geometry();
                setWindowState(Qt::WindowMaximized);
        }
        else{
                setGeometry(_old_rect);
                setWindowState(Qt::WindowNoState);
        }

        int ret = eSHErr_success;
        if(!_cur_file_path.isEmpty()){
                ret = _sdl_mgr->update_displayer(_cur_file_path.toStdString().c_str());
        }
}

void pure_player::open_config_panel()
{

}

//control bar
void pure_player::open_playlist()
{

}

void pure_player::update_display_time()
{
        if(_cur_file_path.isEmpty()){
                return;
        }

        int64_t cur_ts = 0;
        int ret = eSHErr_success;

        if((ret = _sdl_mgr->query_media_progress(_cur_file_path.toStdString().c_str(), &cur_ts)) == eSHErr_success){
                ((control_bar*)_bar_frame[eBar_control])->set_progress_slider_value(cur_ts);
        }
}

void pure_player::modify_media_volume(int val)
{
        int ret = eSHErr_success;
        if(!_cur_file_path.isEmpty()){
                ret = _sdl_mgr->adjust_media_volume(_cur_file_path.toStdString().c_str(), val);
        }
}

void pure_player::modify_media_progress(int val)
{
        int ret = eSHErr_success;
        if(!_cur_file_path.isEmpty()){
                ret = _sdl_mgr->adjust_media_progress(_cur_file_path.toStdString().c_str(), val);
        }
}

void pure_player::operate_media(bool pause)
{
        int ret = eSHErr_success;
        if(!_cur_file_path.isEmpty()){
                ret = _sdl_mgr->operate_display(_cur_file_path.toStdString().c_str(), pause ? eDECOper_pause : eDECOper_keep);
        }
}

void pure_player::change_display_media(const QString& val)
{
        int ret = eSHErr_success;
        if(!_cur_file_path.isEmpty()){
                ret = _sdl_mgr->operate_display(_cur_file_path.toStdString().c_str(), eDECOper_exit);
        }

        _cur_file_path = val;
        _disp_time_timer->stop();

        if((ret = play_media_file(_cur_file_path.toStdString().c_str())) != eSHErr_success){
                return;
        }

        QString title = _cur_file_path.mid(_cur_file_path.lastIndexOf('/') + 1);
        ((title_bar*)_bar_frame[eBar_title])->set_title(title);
}

void  pure_player::open_media_file()
{
        _cur_file_path = QFileDialog::getOpenFileName(this, "Open Media");
        if(_cur_file_path.isEmpty()){
                return;
        }

        int ret = eSHErr_success;
        if((ret = play_media_file(_cur_file_path.toStdString().c_str())) != eSHErr_success){
                QString tips = QString("open file [{0}] failed.").arg(_cur_file_path);
                QMessageBox::information(this, "tips", tips, QMessageBox::Ok, QMessageBox::Ok);
                return;
        }

        QString title = _cur_file_path.mid(_cur_file_path.lastIndexOf('/') + 1);
        ((title_bar*)_bar_frame[eBar_title])->set_title(title);
}

int pure_player::play_media_file(const char* media_file)
{
        int ret = eSHErr_success;
        if((ret = _sdl_mgr->config_displayer(media_file, (void*)winId(), eMediaStream_playback)) != eSHErr_success){
                return ret;
        }

        int64_t duration;
        if(_sdl_mgr->query_media_duration(media_file, &duration) == eSHErr_success){
                ((control_bar*)_bar_frame[eBar_control])->set_progress_slider_range(duration);
        }

        int volume;
        if(_sdl_mgr->query_media_volume(media_file, &volume) == eSHErr_success){
                ((control_bar*)_bar_frame[eBar_control])->set_volume_slider_value(volume);
        }

        if((ret = _sdl_mgr->operate_display(media_file, eDECOper_start)) != eSHErr_success){
                return ret;
        }

        _disp_time_timer->start();
        return eSHErr_success;
}
