#ifndef CONFIG_PANEL_H
#define CONFIG_PANEL_H

#include <QDialog>

namespace Ui {
class config_panel;
}

class config_panel : public QDialog
{
        Q_OBJECT

public:
        explicit config_panel(QWidget *parent = 0);
        ~config_panel();

private:
        Ui::config_panel *ui;
};

#endif // CONFIG_PANEL_H
